<ul class="list-group">
	<li class="list-group-item"><strong>My Account</strong></li>

	@if(Auth::user())
	
	@else 
	  <script>window.location = "/login";</script>
	@endif



	
	<li class="list-group-item" @if(Route::current()->getName()=='profile') style="background-color: #dc3545;border-color: #dc3545;"  @endif >
		<a href="{{ route('profile') }}" @if(Route::current()->getName()=='profile') style="color:#fff" @else style="color:#000" @endif >My Profile</a>
	</li>

	<li class="list-group-item" @if(Route::current()->getName()=='user-projects') style="background-color: #dc3545;border-color: #dc3545;"  @endif >
		<a href="{{ route('user-projects') }}" @if(Route::current()->getName()=='user-projects') style="color:#fff" @else style="color:#000" @endif >My Projects</a>
	</li>

	<li class="list-group-item" @if(Route::current()->getName()=='user-tutorials') style="background-color: #dc3545;border-color: #dc3545;"  @endif >
		<a href="{{ route('user-tutorials') }}" @if(Route::current()->getName()=='user-tutorials') style="color:#fff" @else style="color:#000" @endif >Tutorials</a>
	</li>

	<!-- <li class="list-group-item"><a href="#"  style="color:#000">Projects</a></li> -->
	<!-- <li class="list-group-item"><a href="#"  style="color:#000">Return & Exchange</a></li> -->
	<li class="list-group-item" >
		<a href="{{ route('logout') }}" style="color:#000" >Logout</a>
	</li>
</ul>