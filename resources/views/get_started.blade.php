@extends('template.rush')

@section('content')

<div class="row p-lg-5">

	<div class="row p-lg-3 mx-5">
		<div class="col-md-12" >
			<h1 class="text-center">What would you like to do?</h1>
		</div>
		<div class="card-deck text-center">
			@if(!Auth::check())
			<div class="card" style="width:18rem">
				<a href="{{ route('register') }}">
					<img class="card-img-top card-main p-5" src="{{ URL::asset('images/dev.png')}}" alt="Card image">
					<div class="card-body">
						<h5 class="card-title">SIGN UP</h5>
						<p class="card-text">Signup to use our services</p>
					</div>
				</a>
			</div>
			@endif
			<div class="card" style="width:18rem">
				<a href="{{ route('services')}}">
					<img class="card-img-top card-main p-5" src="{{ URL::asset('images/design.png')}}" alt="Card image">
					<div class="card-body">
						<h5 class="card-title">USE OUR SERVICES</h5>
						<p class="card-text">Select from our wide range or services</p>
					</div>
				</a>
			</div>

			<div class="card" style="width:18rem">
				<a href="{{ route('tutor-form')}}">
					<img class="card-img-top card-main p-5" src="{{ URL::asset('images/writing.png')}}" alt="Card image">
					<div class="card-body">
						<h5 class="card-title">BOOK TUTORIAL</h5>
						<p class="card-text">Book a tutorial with one of our experts</p>
					</div>
				</a>
			</div>
			
			<div class="card" style="width:18rem">
				<a href="#">
					<img class="card-img-top card-main p-5" src="{{ URL::asset('images/papers.png')}}" alt="Card image">
					<div class="card-body">
						<h5 class="card-title">BOOK WORKSHOP</h5>
						<p class="card-text">Book an expert for a workshop</p>
					</div>
				</a>
			</div>

			<div class="card" style="width:18rem">
				<a href="#">
					<img class="card-img-top card-main p-5" src="{{ URL::asset('images/printer.png')}}" alt="Card image">
					<div class="card-body">
						<h5 class="card-title">BECOME A PART OF THE TEAM</h5>
						<p class="card-text">Join the fun</p>
					</div>
				</a>
			</div>
			
		</div>
	</div>
</div>
@stop