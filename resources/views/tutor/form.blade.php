@extends('template.rush')

@section('content')
<script src="https://www.google.com/recaptcha/api.js" async defer></script>


<div class="row p-lg-12">
	<div class="col-md-8">
		<img src="{{ URL::asset('images/paper_option.png')}}" class="img-fluid" alt="Responsive image">
	</div>
	<div class="col-md-4" >
		<div class="col-auto mb-5">
			<form action="{{ route('tutor-submit',[]) }}" method="POST" >
				{{ csrf_field() }}
				
				<h3>Tutorial Details</h3>
				<div class="form-group">
					<label for="location">Venue</label>
					<input type="text" name="location" class="form-control">
				</div>

				<div class="form-group">
					<label for="subject">Topic</label>
					<input type="text" name="subject" class="form-control">
				</div>

				<div class="form-group">					
					<p>Provide a detailed description on the topic</p>
					<TEXTAREA class="form-control" name="details"></TEXTAREA>
				</div>

				
				<div class="row">
					<div class="col-md-4">
						<div class="form-group">					
							<label for="people_count">People Count</label>
							<input type="number" name="people_count" class="form-control" value="1">
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">					
							<label for="hours">Hours</label>
							<input type="number" name="hours" class="form-control" value="1">
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">					
							<label for="datetime">DateTime</label>
							<input type="text" name="date_of_tutorial" class="form-control" id="datetimepicker2" />
						</div>
					</div>
				</div>

				<div class="form-group">					
					<label for="provisions">Provisions</label>
					<select name="provisions" class="form-control">
						<option value="food">Food</option>
						<option value="honorarium">honorarium</option>
						<option value="certificate">certificate</option>
						<option value="none">none</option>
					</select>
				</div>

				<div class="form-group">					
					<p>Contact Number</p>
					<input type="text" name="contact_number" class="form-control">
				</div>
				<!-- <div class="form-group">
					<p>If there are any additional files (ex. drafts, pictures, files) upload those files to be included in the website</p>
					<span class="btn btn-default btn-file">
						<input id="input-2" name="input2[]" type="file" class="file" multiple data-show-upload="true" data-show-caption="true">
					</span>
				</div> -->
				<div class="g-recaptcha" data-sitekey="6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI"></div>
				<button type="submit" class="btn btn-primary">Submit</button>
			</form>
		</div>
	</div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>  
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.7.14/js/bootstrap-datetimepicker.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.7.14/css/bootstrap-datetimepicker.min.css" rel="stylesheet"/>


<script type="text/javascript">
	$('#datetimepicker2').datetimepicker({format : "MM/DD/YYYY hh:mm"});
</script>
@stop