@extends('template.rush')

@section('content')

<div class="row p-lg-12">
	
	<div class="col-md-4 " >
		<div class="card-deck text-center justify-content-center h-80">
			<div class="col-auto mb-5 my-auto">
				<div class="card">
					<a href="{{ route('creatives-picker') }}">						
						<div class="card-body">	
							<div class="row">
								<div class="col-md-12">
									<a href="{{ URL::route('creatives-form',['type'=>'art']) }}" class="btn btn-danger btn-lg active" role="button" aria-pressed="true">Art & Illustrations</a>	
									<br/>
									<br/>
								</div>
								<div class="col-md-12">
									<a href="{{ URL::route('creatives-form',['type'=>'animation']) }}" class="btn btn-danger btn-lg active" role="button" aria-pressed="true">Animation</a>	
									<br/>
									<br/>
								</div>
								<div class="col-md-12">
									<a href="{{ URL::route('creatives-form',['type'=>'print']) }}" class="btn btn-danger btn-lg active" role="button" aria-pressed="true">Print Design</a>	
									<br/>
									<br/>
								</div>
								<div class="col-md-12">
									<a href="{{ URL::route('creatives-form',['type'=>'video']) }}" class="btn btn-danger btn-lg active" role="button" aria-pressed="true">Video Editing</a>	
									<br/>
									<br/>
								</div>
								<div class="col-md-12">
									<a href="{{ URL::route('creatives-form',['type'=>'photo']) }}" class="btn btn-danger btn-lg active" role="button" aria-pressed="true">Photography</a>	
									<br/>
									<br/>
								</div>
								<div class="col-md-12">
									<a href="{{ URL::route('creatives-form',['type'=>'presentation']) }}" class="btn btn-danger btn-lg active" role="button" aria-pressed="true">Presentation</a>	
									<br/>
									<br/>
								</div>
							</div>
								
						</div>
					</a>
				</div>
			</div>
		</div>
		
	</div>
	<div class="col-md-8">
		<img src="{{ URL::asset('images/design_option.png')}}" class="img-fluid" alt="Responsive image">
	</div>
</div>
@stop