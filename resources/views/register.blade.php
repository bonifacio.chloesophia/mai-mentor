@extends('template.rush')

@section('content')
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<div class="row bg-light mx-5 d-flex">

	
			



	<div class="col-md-4">
	
	</div>
	<div class="col-md-4">
		<div class="d-flex justify-content-center">
			<div class="align-self-center top_div_text">Register</div>
		</div>
		@if(Session::has('flash_message'))
			<div class="alert alert-success alert-dismissible">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			{{Session::get('flash_message')}}
			</div>
		@endif

		@if(Session::has('flash_error'))
			<div class="alert alert-danger">{{Session::get('flash_error')}}</div>
		@endif
		<br/>
		<form action="{{ route('register-submit',[]) }}" method="POST" >
			{{ csrf_field() }}		
			<div class="form-group">				
				<label for="first_name">First Name</label>
				<input type="text" name="first_name" class="form-control">
				{{$errors->first('first_name')}}</span>
			</div>
			<div class="form-group">				
				<label for="last_name">Last Name</label>
				<input type="text" name="last_name" class="form-control">
				{{$errors->first('last_name')}}</span>
			</div>
			<div class="form-group">				
				<label for="email">Email</label>
				<input type="email" name="email" class="form-control">
				{{$errors->first('email')}}</span>
			</div>
			<div class="form-group">				
				<label for="contact_no">Contact No</label>
				<input type="text" name="contact_no" class="form-control">
				{{$errors->first('contact_no')}}</span>
			</div>
			<div class="form-group">				
				<label for="password">Password</label>
				<input type="password" name="password" class="form-control">
				{{$errors->first('password')}}</span>
			</div>
			<div class="form-group">				
				<label for="password_confirmation">Repeat Password</label>
				<input type="password" name="password_confirmation" class="form-control">
				{{$errors->first('password_confirmation')}}</span>
			</div>

			<div class="form-group">				
				<label for="type">Are you a..</label>
				<select name="type" class="form-control">
					<option value="student">Student</option>
					<option value="professional">Professional</option>
					<option value="other">Others</option>
				</select>
			</div>

			<br/>
			<div class="g-recaptcha" data-sitekey="6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI"></div>
			<button type="submit" class="btn btn-primary">Submit</button>
		</form>

	</div>
	<div class="col-md-4">
	
	</div>
</div>

@stop