@extends('template.rush')

@section('content')


	<div class="row" >
		

		<div class="col-md-12">

		<div class="panel panel-default"  style="width: 80%;margin: auto;">
			
					
			<div class="panel-body">
				<div class="row">	
						<div class="col-md-3">
							@include('user_nav')
						</div>
						<div class="col-md-9">
							<div class="panel panel-default"  style="width: 100%; margin: auto;">
								<div class="panel-heading" style="padding: 3px 15px">
									<h5>My Profile</h5>			
								</div>
								<div class="panel-body">
									
									<Form method="post" action="{{ route('profile-update') }}"/>
									<!-- <input type="hidden" name="_method" value="PUT"> -->
									{{ @csrf_field() }}
									<div class="row">
										<div class="col-lg-6">
											<label for="first_name">First Name</label>
											<input type="text" name="first_name" class="form-control" placeholder="Enter First name" value="{{ $user->first_name }}">
											<span class="errors" style="color:#FF0000">{{$errors->first('first_name')}}</span>
										</div>
										<div class="col-lg-6">
											<label for="last_name">Last Name</label>
											<input type="text" name="last_name" class="form-control" placeholder="Enter Last name" value="{{  $user->last_name }}">
											<span class="errors" style="color:#FF0000">{{$errors->first('last_name')}}</span>
										</div>
									</div>
									<div class="row">
										<div class="col-lg-6">
											<label for="email">Email</label>
											<input type="email" name="email" class="form-control" placeholder="Enter Email" value="{{ $user->email }}">
											<span class="errors" style="color:#FF0000">{{$errors->first('email')}}</span>
										</div>
										<div class="col-lg-6">
											<label for="contact_no">Contact No</label>
											<input type="text" name="contact_no" class="form-control" placeholder="Enter First name" value="{{  $user->contact_no }}">
											<span class="errors" style="color:#FF0000">{{$errors->first('contact_no')}}</span>
										</div>
									</div>

									<div class="row top10">
										<div class="col-lg-4">
											<br/>
											<input type="submit" class="btn btn-danger btn-sm" value="Submit" />
										</div>
									</div>
									</Form>
								</div>
							</div>
						</div>
				</div>

			</div>
		</div>

		</div>
	</div>



@stop