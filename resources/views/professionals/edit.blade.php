@extends('template.app')

@section('content')

 <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>


<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
		<h1 class="h2">Edit Professional</h1>
			   
	</div>
	<div class="row">
			@if(Session::has('flash_message'))
				<div class="alert alert-success">{{Session::get('flash_message')}}</div>
			@endif

			@if(Session::has('flash_error'))
				<div class="alert alert-danger">{{Session::get('flash_error')}}</div>
			@endif
		</div>
	<div class="row">
	
		<Form method="POST" action="{{ route('professionals.update',[$professional->id]) }}" enctype="multipart/form-data" />
		<input type="hidden" name="_method" value="PUT">
		{{ csrf_field() }}

		
		<div class="form-row ">
			<div class="col">
				<label for="first_name">First Name</label>
				<input type="text" name="first_name" class="form-control" placeholder="Enter First name" value="{{ $professional->user->first_name }}">
				<span class="errors" style="color:#FF0000">{{$errors->first('first_name')}}</span>
			</div>
			<div class="col">
				<label for="last_name">Last Name</label>
				<input type="text" name="last_name" class="form-control" placeholder="Enter Last name" value="{{ $professional->user->last_name }}">
				<span class="errors" style="color:#FF0000">{{$errors->first('last_name')}}</span>
			</div>
		</div>
		<div class="form-row">
			<div class="col">
				<label for="email">Email</label>
				<input type="email" name="email" class="form-control" placeholder="Enter Email" value="{{ $professional->user->email }}"> 
				<span class="errors" style="color:#FF0000">{{$errors->first('email')}}</span>
			</div>
			<div class="col">
				<label for="contact_no">Contact No</label>
				<input type="text" name="contact_no" class="form-control" placeholder="Enter Contact No" value="{{ $professional->user->contact_no }}">
				<span class="errors" style="color:#FF0000">{{$errors->first('contact_no')}}</span>
			</div>
		</div>
		<div class="form-row">
		
			<div class="col">
				<label for="email">Description</label>
				<textarea class="form-control" id="description" name="description">{{ $professional->description }}</textarea>
				<span class="errors" style="color:#FF0000">{{$errors->first('description')}}</span>
			</div>

			 

		</div>
	

		<div class="form-group">
			<div class="form-check">
				<input class="form-check-input" type="checkbox" name="is_available" value="1" @if($professional->is_available) checked="true" @endif>
				<label class="form-check-label" for="gridCheck">
				Available?
				</label>
			</div>
			<div class="form-check">
				<input class="form-check-input" type="checkbox" name="is_active" value="1" @if($professional->is_active) checked="true" @endif>
				<label class="form-check-label" for="gridCheck">
				Active?
				</label>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-4">
				<input type="submit" class="btn btn-primary" onclick="this.disabled=true;this.value='Submitted, please wait...';this.form.submit();" />
			</div>
		</div>
		
		</Form>

	</div>
</main>

<script>

	$(document).ready(function() {
		console.log('asd');

		$('#description').summernote({
        placeholder: 'Description Here',
        tabsize: 2,
        height: 100
      });

	 // 	 $('#description').summernote({
	 // 	 height: 600,

		// });

	});
</script>

@stop