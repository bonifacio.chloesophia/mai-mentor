@extends('template.app')

@section('content')

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
		<h1 class="h2">{{ $professional->user->first_name }} {{ $professional->user->last_name }}</h1>
	</div>
	<div class="row">
			@if(Session::has('flash_message'))
				<div class="alert alert-success">{{Session::get('flash_message')}}</div>
			@endif

			@if(Session::has('flash_error'))
				<div class="alert alert-danger">{{Session::get('flash_error')}}</div>
			@endif
		</div>
	<div class="row">


		<form method="post" action="{{ route('professionals.skills.update',[$professional->id,1]) }}">
			<input type="hidden" name="_method" value="PUT">
			{{ csrf_field() }}
			<table class="table">
			  <thead>
				<tr>
					<th scope="col">Skills</th>
				</tr>
				</thead>
				<tbody>
					@foreach($skills as $x)
					<tr>
						<td>
							<input type="checkbox" name="{{ $x->id }}" value="1" @if($user_skills->where('skill_id',$x->id)->count()>0) checked @endif> {{ $x->name }}	
						</td>
					</tr>
					@endforeach
					
				</tbody>
			</table>
			<input type="submit" value="Submit" class="btn btn-primary">
		</form>
	</div>
</main>


@stop