@extends('template.app')

@section('content')

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
		<h1 class="h2">Professionals</h1>
	</div>
	<div class="row">
			@if(Session::has('flash_message'))
				<div class="alert alert-success">{{Session::get('flash_message')}}</div>
			@endif

			@if(Session::has('flash_error'))
				<div class="alert alert-danger">{{Session::get('flash_error')}}</div>
			@endif
		</div>
	<div class="row">

		 
		<a href="{{ route('professionals.create') }}" role="button" class="btn btn-primary">Add New</a>

		<table class="table">
		  <thead>
			<tr>
				<th scope="col">#</th>
				<th scope="col">Name</th>
				<th scope="col">Email</th>
				<th scope="col">Availability</th>
				<th scope="col">Status</th>
				<th scope="col">Rating</th>
				<th scope="col"></th>
			</tr>
		  </thead>
		  <tbody>

		  	@foreach($professionals as $x)
		  	<tr>
				<th scope="row">{{$x->id}}</th>
				<td>{{$x->user->first_name}} {{$x->user->last_name}}</td>
				<td>{{$x->user->email}}</td>
				<td>
					@if($x->is_available) 
					<span style="height: 10px; width: 10px;  background-color: #00ff00;  border-radius: 50%;  display: inline-block;"></span> Available
					@else
					<span style="height: 10px; width: 10px;  background-color: #ff0000;  border-radius: 50%;  display: inline-block;"></span> Busy
					@endif
				</td>
				<td>
					@if($x->is_active) 
					<span style="height: 10px; width: 10px;  background-color: #00ff00;  border-radius: 50%;  display: inline-block;"></span> Active
					@else
					<span style="height: 10px; width: 10px;  background-color: #ff0000;  border-radius: 50%;  display: inline-block;"></span> Inactive
					@endif
				</td>
				<td></td>
				<td>
					<div class="btn-group" role="group" aria-label="">
						<a href="{{ route('professionals.skills.index',[$x->id])}}"><button type="button" class="btn btn-primary">Skills</button></a>
						<a href="{{ route('professionals.edit',[$x->id])}}"><button type="button" class="btn btn-primary">Edit</button></a>
						<!-- <button type="button" class="btn btn-secondary">Middle</button> -->
						<a href=""><button type="button" class="btn btn-danger">Delete</button></a>
					</div>
				</td>
			</tr>
		  	@endforeach
			
		  </tbody>
		</table>
	

	</div>
</main>


@stop