@extends('template.app')

@section('content')

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
		<h1 class="h2">Settings</h1>

		

			   
	</div>
	<div class="row">
			@if(Session::has('flash_message'))
				<div class="alert alert-success">{{Session::get('flash_message')}}</div>
			@endif

			@if(Session::has('flash_error'))
				<div class="alert alert-danger">{{Session::get('flash_error')}}</div>
			@endif
		</div>
	<div class="row">

	<Form method="POST" action="{{ route('settings.update',[1]) }}" enctype="multipart/form-data" />
	<input type="hidden" name="_method" value="PUT">
		{{ csrf_field() }}
		<div class="row">
			<div class="col-lg-4">
				<Label for="{{ $company_logo->name }}" > {{$company_logo->description}}</Label>
				<image src="/uploads/{{$company_logo->value}}" style="height:100px;width: 300px"/>
				<input type="file" name="photo">
				<input class="form-control" type="hidden" name="settings_id[]" value="{{ $company_logo->id }}" />
				<input type="hidden" name="settings_name[]" value="{{ $company_logo->name }}" />
				<span class="errors" style="color:#FF0000">{{$errors->first($company_logo->name)}}</span>
			</div>
		</div>
		<div class="row">	
			<div class="col-lg-4">
				<Label for="{{ $software_name->name }}" >{{ $software_name->description}}</Label>
				<input class="form-control" type="text" name="{{ $software_name->name }}" value="{{ $software_name->value }}" placeholder="{{ $software_name->description }}" /> 
				<input type="hidden" name="settings_id[]" value="{{ $software_name->id }}" />
				<input type="hidden" name="settings_name[]" value="{{ $software_name->name }}" />
				<span class="errors" style="color:#FF0000">{{$errors->first($software_name->name)}}</span>
			</div>

			<div class="col-lg-4">
				<Label for="{{ $company_name->name }}" >{{ $company_name->description}}</Label>
				<input class="form-control" type="text" name="{{ $company_name->name }}" value="{{ $company_name->value }}" placeholder="{{ $company_name->description }}" />
				<input type="hidden" name="settings_id[]" value="{{ $company_name->id }}" />
				<input type="hidden" name="settings_name[]" value="{{ $company_name->name }}" />
				<span class="errors" style="color:#FF0000">{{$errors->first($company_name->name)}}</span>
			</div>

			<div class="col-lg-4">
				<Label for="{{ $company_address->name }}" > {{$company_address->description}}</Label>
				<input class="form-control" type="text" name="{{ $company_address->name }}" value="{{ $company_address->value }}" placeholder="{{ $company_address->description }}" />
				<input type="hidden" name="settings_id[]" value="{{ $company_address->id }}" />
				<input type="hidden" name="settings_name[]" value="{{ $company_address->name }}" />
				<span class="errors" style="color:#FF0000">{{$errors->first($company_address->name)}}</span>
			</div>

			<div class="col-lg-4">
				<Label for="{{ $company_phone->name }}" > {{$company_phone->description}}</Label>
				<input class="form-control" type="text" name="{{ $company_phone->name }}" value="{{ $company_phone->value }}" placeholder="{{ $company_phone->description }}" />
				<input type="hidden" name="settings_id[]" value="{{ $company_phone->id }}" />
				<input type="hidden" name="settings_name[]" value="{{ $company_phone->name }}" />
				<span class="errors" style="color:#FF0000">{{$errors->first($company_phone->name)}}</span>
			</div>

			<div class="col-lg-4">
				<Label for="{{ $company_email->name }}" > {{$company_email->description}}</Label>
				<input class="form-control" type="text" name="{{ $company_email->name }}" value="{{ $company_email->value }}" placeholder="{{ $company_email->description }}" />
				<input type="hidden" name="settings_id[]" value="{{ $company_email->id }}" />
				<input type="hidden" name="settings_name[]" value="{{ $company_email->name }}" />
				<span class="errors" style="color:#FF0000">{{$errors->first($company_email->name)}}</span>
			</div>

					
		</div>

		<br/>



		<div class="row top10">
			<div class="col-lg-4">
				<input type="submit" class="btn btn-primary" onclick="this.disabled=true;this.value='Submitted, please wait...';this.form.submit();" />
			</div>
		</div>
		
		</Form>

	</div>
</main>
@stop