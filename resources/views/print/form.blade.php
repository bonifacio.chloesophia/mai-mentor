@extends('template.rush')

@section('content')
<script src="https://www.google.com/recaptcha/api.js" async defer></script>


<div class="row p-lg-12">
	<div class="col-md-8">
		<img src="{{ URL::asset('images/print_option.png')}}" class="img-fluid" alt="Responsive image">
	</div>
	<div class="col-md-4" >
		<div class="col-auto mb-5">
			<form action="{{ route('print-submit',[]) }}" method="POST" enctype="multipart/form-data">
				{{ csrf_field() }}
				
			<!-- 	<div class="form-group">
					<label for="title">Project Title</label>
					<input type="text" name="title" class="form-control">
				</div>

				
				<div class="form-group">					
					<p>Provide a detailed description on what you want your project to look like. Add all needed specifications and features.</p>
					<TEXTAREA class="form-control" name="description"></TEXTAREA>
				</div> -->

				<div class="form-group">
					<label for="title">Upload File</label>
					<br/>
					<span class="btn btn-default btn-file">
						<input id="file" name="file" type="file" class="file" data-show-upload="true" data-show-caption="true">
					</span>
				</div>

				<div class="row">
					<div class="col-md-6">
						<label for="paper_size">Paper Size</label>
						<select name="paper_size" class="form-control">
							<option value="letter">Letter (8.5 x 11)</option>
							<option value="legal">Legal (8.5 x 14)</option>
							<option value="a4">A4 (8.3 x 11.7)</option>
							<option value="others">Others</option>
						</select>
					</div>
					<div class="col-md-6">
						<label for="paper_type">Paper Type</label>
						<select name="paper_type" class="form-control">
							<option value="plain">Plain</option>
							<option value="glossy">Glossy/Photo</option>
						</select>
						
					</div>
				</div>

				<div class="row">
					<div class="col-md-6">
						<label for="copies">Copies</label>
						<input type="number" name="copies" class="form-control" step="1" value="1">
					</div>
					<div class="col-md-6">
						<label for="title">Color</label>
						<select name="color" class="form-control">
							<option value="color">Colored</option>
							<option value="greyscale">Greyscale</option>
						</select>
					</div>
				</div>




				<div class="form-group">
					
				</div>

				<div class="form-group">					
					<p>Additional Instructions</p>
					<TEXTAREA class="form-control" name="instructions"></TEXTAREA>
				</div>


				<div class="g-recaptcha" data-sitekey="6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI"></div>
				<button type="submit" class="btn btn-primary">Submit</button>
			</form>
		</div>
	</div>
</div>
@stop