@extends('template.rush')

@section('content')

<div class="row p-lg-12">
	<div class="col-md-8">
		<img src="{{ URL::asset('images/programming_option.png')}}" class="img-fluid" alt="Responsive image">
	</div>
	<div class="col-md-4" >
		<div class="card-deck text-center justify-content-center h-100">
			<div class="col-auto mb-5 my-auto">
				<div class="card" style="height: 15rem">
					<a href="{{ route('development-picker') }}">						
						<div class="card-body">	
							<div class="row">
								<div class="col-md-12">
									<a href="{{ URL::route('development-form',['type'=>'web']) }}" class="btn btn-danger btn-lg active" role="button" aria-pressed="true">Web Development</a>	
									<br/>
									<br/>
								</div>
								<div class="col-md-12">
									<a href="{{ URL::route('development-form',['type'=>'app']) }}" class="btn btn-danger btn-lg active" role="button" aria-pressed="true">App Development</a>	
									<br/>
									<br/>
								</div>
								<div class="col-md-12">
									<a href="{{ URL::route('development-form',['type'=>'software']) }}" class="btn btn-danger btn-lg active" role="button" aria-pressed="true">Software Development</a>	
									<br/>
									<br/>
								</div>
							</div>
								
						</div>
					</a>
				</div>
			</div>
		</div>
		
	</div>
</div>
@stop