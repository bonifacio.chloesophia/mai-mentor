@extends('template.rush')

@section('content')

<div class="row bg-light mx-5 d-flex">

	
		<div class="col-md-12">
			<div class="d-flex justify-content-center">
				<div class="align-self-center top_div_text">We Also Offer..</div>
			</div>
		</div>
		
		<div class="col-md-12">
			<br/>
			<div class="card-deck text-center justify-content-center">
				<div class="col-auto mb-3">
					<div class="card">
						<a href="{{ route('development-picker') }}">
							<img class="card-img-top card-main p-5" src="{{ URL::asset('images/dev.png')}}" alt="Card image">
							<div class="card-body">								
							</div>
						</a>
					</div>
					<div class="d-flex justify-content-center">
						<a href="{{ route('development-picker') }}">
						<div class="align-self-center top_div_text">Development</div>
						</a>
					</div>
				</div>
			
				<div class="col-auto mb-3">
					<div class="card">
						<a href="{{ route('creatives-picker') }}">
							<img class="card-img-top card-main p-5" src="{{ URL::asset('images/design.png')}}" alt="Card image">
							<div class="card-body">								
							</div>
						</a>
					</div>
					<div class="d-flex justify-content-center">
						<div class="align-self-center top_div_text">Creatives</div>
					</div>
				</div>
		
				<div class="col-auto mb-3">
					<div class="card">
						<a href="{{ route('writing-picker') }}">
							<img class="card-img-top card-main p-5" src="{{ URL::asset('images/writing.png')}}" alt="Card image">
							<div class="card-body">								
							</div>
						</a>
					</div>
					<div class="d-flex justify-content-center">
						<div class="align-self-center top_div_text">Writing</div>
					</div>
				</div>
			</div>
			<div class="card-deck text-center justify-content-center">
				<div class="col-auto mb-3">
					<div class="card">
						<a href="{{ route('paper-picker') }}">
							<img class="card-img-top card-main p-5" src="{{ URL::asset('images/papers.png')}}" alt="Card image">
							<div class="card-body">								
							</div>
						</a>
					</div>
					<div class="d-flex justify-content-center">
						<div class="align-self-center top_div_text">Papers</div>
					</div>
				</div>
			
				<div class="col-auto mb-3">
					<div class="card">
						<a href="{{ route('print-form') }}">
							<img class="card-img-top card-main p-5" src="{{ URL::asset('images/printer.png')}}" alt="Card image">
							<div class="card-body">								
							</div>
						</a>
					</div>
					<div class="d-flex justify-content-center">
						<div class="align-self-center top_div_text">Printing</div>
					</div>
				</div>
			</div>
		</div>
		
	
</div>
@stop