@extends('template.app')

@section('content')

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
		<h1 class="h2">Projects</h1>
	</div>
	<div class="row">
			@if(Session::has('flash_message'))
				<div class="alert alert-success">{{Session::get('flash_message')}}</div>
			@endif

			@if(Session::has('flash_error'))
				<div class="alert alert-danger">{{Session::get('flash_error')}}</div>
			@endif
		</div>
	<div class="row">

		 
		<a href="{{ route('projects.create') }}" role="button" class="btn btn-primary">Add New</a>

		<table class="table">
		  <thead>
			<tr>
				<th scope="col">#</th>
				<th scope="col">Type</th>
				<th scope="col">Title</th>
				<th scope="col">Client</th>
				<th scope="col">Status</th>
				<th scope="col"></th>
			</tr>
		  </thead>
		  <tbody>

		  	@foreach($projects as $x)
		  	<tr>
				<th scope="row">{{$x->id}}</th>
				<th scope="row">{{$x->type}}</th>
				<th scope="row">{{$x->title}}</th>
				<th scope="row">{{$x->user->first_name}} {{$x->user->last_name}}</th>
				<td>
					@if($x->status=='active') 
					<span style="height: 10px; width: 10px;  background-color: #00ff00;  border-radius: 50%;  display: inline-block;"></span> Active
					@elseif($x->status=='review') 
					<span style="height: 10px; width: 10px;  background-color: #5bc0de;  border-radius: 50%;  display: inline-block;"></span> For Review
					@elseif($x->status=='inactive') 
					<span style="height: 10px; width: 10px;  background-color: #FF0000;  border-radius: 50%;  display: inline-block;"></span> Inactive
					@elseif($x->status=='wip') 
					<span style="height: 10px; width: 10px;  background-color: #FF0000;  border-radius: 50%;  display: inline-block;"></span> WIP
					@else
					<span style="height: 10px; width: 10px;  background-color: #00FF00;  border-radius: 50%;  display: inline-block;"></span> completed
					@endif
				</td>
				<td>
					<div class="btn-group" role="group" aria-label="">
						<a href="{{ route('projects.milestones.index',[$x->id])}}"><button type="button" class="btn btn-primary">Milestone</button></a>
						<a href="{{ route('projects.professionals.index',[$x->id])}}"><button type="button" class="btn btn-primary">Team</button></a>
						<a href="{{ route('projects.edit',[$x->id])}}"><button type="button" class="btn btn-primary">Edit</button></a>
						<!-- <button type="button" class="btn btn-secondary">Middle</button> -->
						<a href=""><button type="button" class="btn btn-danger">Delete</button></a>
					</div>
				</td>
			</tr>
		  	@endforeach
			
		  </tbody>
		</table>
		<div style="text-align: center">
			{{ $projects->appends(
				[
					
				]

				)->links() }}
		</div>
	

	</div>
</main>


@stop