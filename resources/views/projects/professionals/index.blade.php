@extends('template.app')

@section('content')

 <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
		<h1 class="h2">Project : {{$project->title}}</h1>

		<h3 class="h3">Status : 
		@if($project->status=='active') 
		<span class="badge badge-primary">Active</span>
		@elseif($project->status=='review') 
		<span class="badge badge-info">For Review</span>
		@elseif($project->status=='inactive') 
		<span class="badge badge-danger">Inactive</span>
		@elseif($project->status=='wip') 
		<span class="badge badge-info">WIP</span>
		@else
		<span class="badge badge-success">Completed</span>
		@endif
		</h3>
	</div>
	<div class="row">
		@if(Session::has('flash_message'))
			<div class="alert alert-success">{{Session::get('flash_message')}}</div>
		@endif

		@if(Session::has('flash_error'))
			<div class="alert alert-danger">{{Session::get('flash_error')}}</div>
		@endif
	</div>

	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
		<h5>Type : {{$project->type}}</h5>
	</div>
	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
		<h5>Description</h5>
	</div>
	<div class="row">
		<div class="col-md-12">
		{!!$project->description!!}
		</div>
	</div>


	<div class="row">
		<div class="col-md-4">
			<Form method="POST" action="{{ route('projects.professionals.store',[$project->id]) }}" enctype="multipart/form-data" />
			{{ csrf_field() }}
			<div class="row ">
				<div class="col-lg-12">
					<label for="title">Professional</label>	<br/>		
					<select name="user_id" class="js-example-basic-single form-control" id="first_name">
						@foreach($professionals as $x)
							<option value="{{$x->user->id}}">{{$x->user->first_name}} {{$x->user->last_name}}</option>
						@endforeach
					</select>
					<span class="errors" style="color:#FF0000">{{$errors->first('title')}}</span>
				</div>
				</div>
				<div class="row ">
				<div class="col-lg-12">
					<input type="submit" value="Add to Team" class="btn btn-primary" onclick="this.disabled=true;this.value='Submitted, please wait...';this.form.submit();" />
				
				</div>
			</div>
			</Form>
		</div>
		<div class="col-md-4">
			<canvas class="my-4" id="skilltypeChart"></canvas>
		</div>
		 
		<div class="col-md-4">
			<canvas class="my-4" id="skillChart"></canvas>
		</div>
		 
	<!-- 	<a href="{{ route('projects.create') }}" role="button" class="btn btn-primary">Assign Member</a> -->

	</div>
	<div class="row">
		<div class="col-md-12">
		<table class="table">
		  <thead>
			<tr>
				<th scope="col">Name</th>
				<th scope="col">Skills</th>
				<th scope="col"></th>
			</tr>
		  </thead>
		  <tbody>
		  @php
		  	$codingCount = 0;
		  	$designCount = 0;
		  	$qaCount = 0;
		  	$docsCount = 0;
		  @endphp
		  @foreach($pp as $x)
		  	<tr>
		  		<td>{{$x->user->first_name}} {{$x->user->last_name}}</td>
		  		<td>
		  		@foreach($x->user->userSkills as $w)
		  			@if($w->skill->type=='coding')
		  			@php
		  			$codingCount=$codingCount+1;
		  			@endphp
		  			<span class="badge badge-success">{{$w->skill->name}}</span>		  			
		  			@elseif($w->skill->type=='design')		  			
		  			@php
		  			$designCount=$designCount+1;
		  			@endphp
		  			<span class="badge badge-warning">{{$w->skill->name}}</span>
		  			@elseif($w->skill->type=='qa')		  			
		  			@php
		  			$qaCount=$qaCount+1;
		  			@endphp
		  			<span class="badge badge-primary">{{$w->skill->name}}</span>
		  			@else
		  			@php
		  			$docsCount=$docsCount+1;
		  			@endphp		  			
		  			<span class="badge badge-danger">{{$w->skill->name}}</span>
		  			@endif
		  			
		  		@endforeach

		  		</td>
		  		<td>
		  		<div class="btn-group" role="group" aria-label="">
				
					<a href="{{ route('projects.professionals.delete',[$project->id,$x->user->id]) }}"><button type="button" class="btn btn-danger">Remove</button></a>
				</div>
				</td>
		  	</tr>
		  @endforeach

			
		  </tbody>
		</table>
		</div>
	</div>


		
</main>
<link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>


<script>
	$(document).ready(function() {
	    $('.js-example-basic-single').select2();
	});

</script>

<script src="{{ URL::asset('js/chart.js') }}"></script>
<script>
var ctx = document.getElementById("skilltypeChart").getContext('2d');
var myChart = new Chart(ctx, {
	type: 'pie',	
	data: {
		datasets: [{
		data: [{{$codingCount}}, {{$designCount}}, {{$qaCount}},{{$docsCount}}],        
		// data: [1, 2, 3,4],        
		 backgroundColor:  [
		 "rgba(0, 255, 0, 0.5)",
		 "rgba(255, 159, 64, 0.5)",
		 "rgba(0, 0, 255, 0.5)",
		 "rgba(255, 0, 0, 0.5)",
		 "rgba(54, 162, 235, 0.5)",
		 "rgba(153, 102, 255, 0.5)",
		 "rgba(201, 203, 207, 0.5)"],
		borderColor:  [
		"rgba(0, 255, 0, 1)",
		"rgba(255, 159, 64, 1)",
		"rgba(0, 0, 255, 1)",
		"rgba(255, 0, 0, 1)",
		"rgba(54, 162, 235, 1)",
		"rgba(153, 102, 255, 1)",
		"rgba(201, 203, 207, 1)"]
	}],
	labels: ['coding','design','qa','docs' ],   
   
	},
	 options: {
		tooltips: {
			mode: 'index',
			intersect: false
		},
		legend: {
    		position: 'bottom'
 		},
	}
});


var ctx2 = document.getElementById("skillChart").getContext('2d');
var myChart2 = new Chart(ctx2, {
	type: 'bar',	
	data: {
		datasets: [{
			label: 'Skill Sets',
		data: [

			@foreach($skills as $x)

				{{$skills_collection->where('name',$x->name)->count() }},
				
			@endforeach

		],        
		  backgroundColor: [
        'rgba(255, 99, 132, 0.2)',
        'rgba(54, 162, 235, 0.2)',
        'rgba(255, 206, 86, 0.2)',
        'rgba(75, 192, 192, 0.2)',
        'rgba(153, 102, 255, 0.2)',
        'rgba(255, 159, 64, 0.2)',
        'rgba(255, 99, 132, 0.2)',
        'rgba(54, 162, 235, 0.2)',
        'rgba(255, 206, 86, 0.2)',
        'rgba(75, 192, 192, 0.2)',
        'rgba(153, 102, 255, 0.2)',
        'rgba(255, 159, 64, 0.2)'
      ],
      borderColor: [
        'rgba(255,99,132,1)',
        'rgba(54, 162, 235, 1)',
        'rgba(255, 206, 86, 1)',
        'rgba(75, 192, 192, 1)',
        'rgba(153, 102, 255, 1)',
        'rgba(255, 159, 64, 1)',
        'rgba(255,99,132,1)',
        'rgba(54, 162, 235, 1)',
        'rgba(255, 206, 86, 1)',
        'rgba(75, 192, 192, 1)',
        'rgba(153, 102, 255, 1)',
        'rgba(255, 159, 64, 1)'
      ],
      borderWidth: 1
	}],
	labels: [
	@foreach($skills as $x)
		'{{$x->name}}',
	@endforeach
	],   
   
	},
	 options: {
	 	scales: {
		        yAxes: [{
		            ticks: {
		                beginAtZero: true
		            }
		        }]
		    },
		tooltips: {
			mode: 'index',
			intersect: false
		},
		legend: {
    		position: 'bottom'
 		},
	}
});

</script>


@stop