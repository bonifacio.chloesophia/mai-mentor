@extends('template.app')

@section('content')

 <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>


<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
		<h1 class="h2">Add Milestone to {{$project->title}}</h1>
			   
	</div>
	<div class="row">
			@if(Session::has('flash_message'))
				<div class="alert alert-success">{{Session::get('flash_message')}}</div>
			@endif

			@if(Session::has('flash_error'))
				<div class="alert alert-danger">{{Session::get('flash_error')}}</div>
			@endif
		</div>
	<div class="row">


	<Form method="POST" action="{{ route('projects.milestones.store',$project->id) }}" enctype="multipart/form-data" />
	
		{{ csrf_field() }}

		
		<div class="form-row ">
			<div class="col">
				<label for="title">Name</label>	<br/>		
				<input type="text" name="name" class="form-control">
				<span class="errors" style="color:#FF0000">{{$errors->first('title')}}</span>
			</div>
		</div>


		<div class="form-row ">
			<div class="col">
				<label for="start">START</label>	<br/>		
				<input type="date" name="start" class="form-control">
				<span class="errors" style="color:#FF0000">{{$errors->first('start')}}</span>
			</div>
		</div>


		<div class="form-row ">
			<div class="col">
				<label for="end">END</label>	<br/>		
				<input type="date" name="end" class="form-control">
				<span class="errors" style="color:#FF0000">{{$errors->first('end')}}</span>
			</div>
		</div>


		
		<div class="form-row ">
			<div class="col">
				<label for="title">Professional</label>	<br/>		
				<select name="user_id" class="js-example-basic-single form-control" id="first_name">
					<option></option>
					@foreach($pp as $x)
						<option value="{{$x->user->id}}">{{$x->user->first_name}} {{$x->user->last_name}}</option>
					@endforeach
				</select>
				<span class="errors" style="color:#FF0000">{{$errors->first('user_id')}}</span>
			</div>
		</div>

	

	
		<div class="row">
			<div class="col-lg-4">
			<br/>
				<input type="submit" class="btn btn-primary" onclick="this.disabled=true;this.value='Submitted, please wait...';this.form.submit();" />
			</div>
		</div>
		
		</Form>

	</div>
</main>
<link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
<script>
	$(document).ready(function() {
	    $('.js-example-basic-single').select2();
	});

	$(document).ready(function() {
	

		$('#description').summernote({
        placeholder: 'Description Here',
        tabsize: 2,
        height: 100
      });

	 // 	 $('#description').summernote({
	 // 	 height: 600,

		// });

	});
</script>

@stop