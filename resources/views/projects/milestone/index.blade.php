@extends('template.app')

@section('content')

 <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
		<h1 class="h2">Project : {{$project->title}}</h1>

		<h3 class="h3">Status : 
		@if($project->status=='active') 
		<span class="badge badge-primary">Active</span>
		@elseif($project->status=='review') 
		<span class="badge badge-info">For Review</span>
		@elseif($project->status=='inactive') 
		<span class="badge badge-danger">Inactive</span>
		@elseif($project->status=='wip') 
		<span class="badge badge-info">WIP</span>
		@else
		<span class="badge badge-success">Completed</span>
		@endif
		</h3>
	</div>
	<div class="row">
		@if(Session::has('flash_message'))
			<div class="alert alert-success">{{Session::get('flash_message')}}</div>
		@endif

		@if(Session::has('flash_error'))
			<div class="alert alert-danger">{{Session::get('flash_error')}}</div>
		@endif
	</div>

	
	<div class="row">
		<div class="col-md-4">
			<a href="{{route('projects.milestones.create',$project->id)}}"> <button class="btn btn-danger">Add Milestone</button></a>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<table class="table">
				<tr>
					<th>Milestone</th>
					<th>Date Start</th>
					<th>Date End</th>
					<th>Professional</th>
					<th>Dev status</th>
					<th>Milestone Status</th>
					<th></th>
				</tr>
				@foreach($pm as $x)
					<tr>
						<td>{{$x->name}}</td>
						<td>{{$x->start}}</td>
						<td>
						@if(Carbon\Carbon::parse($x->end)->year>1)
						{{$x->end}}
						@endif
						 </td>
					
						@if($x->professionals)
							<td>{{$x->professionals->user->first_name}} {{$x->professionals->user->last_name}}</td>
						@else
							<td></td>
						@endif
						<td>
							@if($x->done==1)
								done
							@else
								not done
							@endif
						</td>
						<td>
							@if(Carbon\Carbon::parse($x->end)->year > 1)
								@if(Carbon\Carbon::parse($x->end)->diffInDays(Carbon\Carbon::now()) <= 0 && $x->done==1)
								done
								@elseif(Carbon\Carbon::parse($x->end)->diffInDays(Carbon\Carbon::now()) <= 0 && $x->done!=1)
								overdue
								@else
								<!-- {{Carbon\Carbon::parse($x->end)->diffInDays(Carbon\Carbon::now())}} -->
								pending
								@endif
							@else
							done
							@endif
							
						</td>
						<td>
				  		<div class="btn-group" role="group" aria-label="">
				  		
				  			<a href="{{ route('projects.milestones.done',[$project->id,$x->id]) }}"><button type="button" class="btn btn-success">Toggle Done</button></a>
							

							<a href="{{ route('projects.milestones.delete',[$project->id,$x->id]) }}"><button type="button" class="btn btn-danger">Remove</button></a>
						</div>
						</td>
					</tr>
				@endforeach
			</table>
		</div>
	</div>



		
</main>
<link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>


<script>
	$(document).ready(function() {
	    $('.js-example-basic-single').select2();
	});

</script>


@stop