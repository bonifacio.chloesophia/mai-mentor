<!doctype html>
<html lang="en">
  <head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="icon" href="../../../../favicon.ico">

	<title>{{ Session::get('software_name') }}</title>

	<!-- Bootstrap core CSS -->
	<link href="{{ URL::asset('css/bootstrap-material-design.min.css') }}" rel="stylesheet">
	<script src="https://code.jquery.com/jquery-3.4.1.min.js" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>    
	<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.16/dist/summernote-bs4.min.css" rel="stylesheet">
	<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.16/dist/summernote-bs4.min.js"></script>



	<!-- Custom styles for this template -->
	<!-- <link href="dashboard.css" rel="stylesheet"> -->
  </head>

  <body>
	<nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0">
	  <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="{{ route('dashboard') }}">{{ Session::get('company_name') }}</a>
	  
	  <ul class="navbar-nav px-3">
		<li class="nav-item text-nowrap">
		  <a class="nav-link" href="{{ route('logout') }}">Sign out</a>
		</li>
	  </ul>
	</nav>

	<div class="container-fluid" style="min-height: 100vh">
	  <div class="row">
		<nav class="col-md-2 d-none d-md-block bg-light sidebar" style=" min-height: calc(100vh - 50px);">
		  <div class="sidebar-sticky">
			<ul class="nav flex-column">
				<li class="nav-item">
					<a class="nav-link active" href="{{ route('dashboard') }}">
					<span data-feather="home"></span>
					Dashboard <span class="sr-only">(current)</span>
					</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="{{ route('projects.index') }}">
					<span data-feather="folder"></span>
					Projects
					</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="{{ route('clients.index') }}">
					<span data-feather="users"></span>
					Clients
					</a>
				</li>

				<li class="nav-item">
					<a class="nav-link" href="{{ route('professionals.index') }}">
					<span data-feather="award"></span>
					Professionals
					</a>
				</li>

			<!--   <li class="nav-item">
				<a class="nav-link" href="#">
				  <span data-feather="shopping-cart"></span>
				  Products
				</a>
			  </li>
			 
			  <li class="nav-item">
				<a class="nav-link" href="#">
				  <span data-feather="bar-chart-2"></span>
				  Reports
				</a>
			  </li>
			  <li class="nav-item">
				<a class="nav-link" href="#">
				  <span data-feather="layers"></span>
				  Integrations
				</a>
			  </li> -->
			</ul>

			<h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
			  <span>Settings</span>
			 <!--  <a class="d-flex align-items-center text-muted" href="#">
				<span data-feather="plus-circle"></span>
			  </a> -->
			</h6>
			<ul class="nav flex-column mb-2">
			 <!--  <li class="nav-item">
				<a class="nav-link" href="#">
				  <span data-feather="file-text"></span>
					Application
				</a>
			  </li>	 -->		
			  <li class="nav-item">
				<a class="nav-link" href="{{ route('settings.edit',[1]) }}">
				  <span data-feather="file-text"></span>
					Settings
				</a>
			  </li>
			  <li class="nav-item">
				<a class="nav-link" href="{{ route('skills.index') }}">
				  <span data-feather="file-text"></span>
					Skills
				</a>
			  </li>
			  <li class="nav-item">
				<a class="nav-link" href="{{ route('logout') }}">
				  <span data-feather="file-text"></span>
					Logout
				</a>
			  </li>
			</ul>
		  </div>
		</nav>

		@yield('content')
		
	  </div>
	</div>

	<!-- Bootstrap core JavaScript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	
	<!-- <script>window.jQuery || document.write('<script src="../../../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
	<script src="../../../../assets/js/vendor/popper.min.js"></script>
	<script src="../../../../dist/js/bootstrap.min.js"></script> -->

	<!-- Icons -->
	<script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>
	<script>
	  feather.replace()
	</script>

	<!-- Graphs -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js"></script>
	<script>
	  var ctx = document.getElementById("myChart");
	  var myChart = new Chart(ctx, {
		type: 'line',
		data: {
		  labels: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
		  datasets: [{
			data: [15339, 21345, 18483, 24003, 23489, 24092, 12034],
			lineTension: 0,
			backgroundColor: 'transparent',
			borderColor: '#007bff',
			borderWidth: 4,
			pointBackgroundColor: '#007bff'
		  }]
		},
		options: {
		  scales: {
			yAxes: [{
			  ticks: {
				beginAtZero: false
			  }
			}]
		  },
		  legend: {
			display: false,
		  }
		}
	  });
	</script>
  </body>
</html>
