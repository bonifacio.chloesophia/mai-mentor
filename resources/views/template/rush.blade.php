<!DOCTYPE html>
<html>
	<head>
		<title>mAI mentor</title>
		<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
		<link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons">        
		<link href="{{ URL::asset('fontawesome/css/all.css') }}" rel="stylesheet">
		<link href="{{ URL::asset('css/bootstrap-material-design.min.css') }}" rel="stylesheet">
		<link href="{{ URL::asset('css/style.css') }}" rel="stylesheet">
	</head>
	<body>

<!-- NAV START -->
	<nav class="site-header sticky-top py-1">
	  <div class="row flex-nowrap  align-items-center">
		  <div class="col-4 pt-1">
			<a class="navbar-brand" href="{{ URL::route('index')}}">
				<img src="{{ URL::asset('images/logo_colored.png') }}" width="30" height="30" alt="">
				<span style="color:#ff0000">mAI mentor</span>
			</a>
		  </div>
		  <div class="col-4 pt-1">
		  </div>
		  <div class="col-4 d-flex justify-content-end">
		  	@if(Auth::check())
		  		<span class="px-2 py-1"><p>Hi! {{ Auth::user()->first_name}}</p></span>
		  		<a href="{{ route('profile')}}"><button type="button" class="btn btn-danger">View My Profile</button></a>
				<a href="{{ route('logout') }}"><button type="button" class="btn btn-outline-danger">Logout</button></a>
		  	@else
		  		<a href="{{ route('login')}}"><button type="button" class="btn btn-danger">Login</button></a>
				<a href="{{ route('register') }}"><button type="button" class="btn btn-outline-danger">Sign-up</button></a>
		  	@endif
			
		  </div>
		</div>
	</nav>
<!-- NAV END -->
	

	@yield('content')


	<footer class="footer">
	  <div class="row px-5 py-4">
		<div class="col-6 col-md">
		<!--   <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="d-block mb-2"><circle cx="12" cy="12" r="10"></circle><line x1="14.31" y1="8" x2="20.05" y2="17.94"></line><line x1="9.69" y1="8" x2="21.17" y2="8"></line><line x1="7.38" y1="12" x2="13.12" y2="2.06"></line><line x1="9.69" y1="16" x2="3.95" y2="6.06"></line><line x1="14.31" y1="16" x2="2.83" y2="16"></line><line x1="16.62" y1="12" x2="10.88" y2="21.94"></line></svg>
		  <small class="d-block mb-3 text-muted">&copy; 2017-2018</small>
 -->		
		 <h5 class="footer-grey">mAI mentor</h5>
		  <ul class="list-unstyled text-small">
			<li><a class="text-muted" href="#">About</a></li>
			<li><a class="text-muted" href="#">Terms of Service</a></li>
			<li><a class="text-muted" href="#">Contact Us</a></li>
			<li><a class="text-muted" href="#">Another one</a></li>
			<li><a class="text-muted" href="#">Last time</a></li>
		  </ul>
		</div>
		<div class="col-6 col-md">
		  <h5 class="footer-grey">Services</h5>
		  <ul class="list-unstyled text-small">
			<li><a class="text-muted" href="#">Development</a></li>
			<li><a class="text-muted" href="#">Design</a></li>
			<li><a class="text-muted" href="#">Writing</a></li>
			<li><a class="text-muted" href="#">Database</a></li>
			<li><a class="text-muted" href="#">Networking</a></li>
			<li><a class="text-muted" href="#">Projects</a></li>
		  </ul>
		</div>
		<div class="col-6 col-md">
		  <h5 class="footer-grey">Our Partners</h5>
		  <ul class="list-unstyled text-small">
			<li><a class="text-muted" href="#">Copytrade</a></li>
			<li><a class="text-muted" href="#">TNC</a></li>
			<li><a class="text-muted" href="#">Aguora IT Solutions</a></li>
		  </ul>
		</div>
		<div class="col-6 col-md">
	<!-- 	  <h5>Resources</h5>
		  <ul class="list-unstyled text-small">
			<li><a class="text-muted" href="#">Business</a></li>
			<li><a class="text-muted" href="#">Education</a></li>
			<li><a class="text-muted" href="#">Government</a></li>
			<li><a class="text-muted" href="#">Gaming</a></li>
		  </ul> -->
		</div>
		<div class="col-6 col-md my-2">
			<img src="{{ URL::asset('images/logo_black.png') }}" style="width: 100px;height: 100px;">
			<small class="d-block mb-3 text-muted">&copy; mAI mentor 2017-2018</small>
		</div>
	  </div>
	</footer>


	<!-- Bootstrap core JavaScript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="{{ URL::asset('js/jquery-3.3.1.js') }}"></script>
	<script src="{{ URL::asset('js/pooper.js') }}"></script>
	<script src="{{ URL::asset('js/bootstrap-material-design.js')}}"></script>

	<script type="text/javascript">
		@if(Session::has('flash_message'))
			Swal.fire({
			  title: 'Success!',
			  text: '{{Session::get('flash_message')}}',
			  type: 'success',
			  confirmButtonText: 'Ok'
			})
		@endif
	</script>
 
  </body>

</html>
