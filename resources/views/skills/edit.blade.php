@extends('template.app')

@section('content')

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
		<h1 class="h2">Edit Skill</h1>
			   
	</div>
	<div class="row">
			@if(Session::has('flash_message'))
				<div class="alert alert-success">{{Session::get('flash_message')}}</div>
			@endif

			@if(Session::has('flash_error'))
				<div class="alert alert-danger">{{Session::get('flash_error')}}</div>
			@endif
		</div>
	<div class="row">

	<Form method="POST" action="{{ route('skills.update',[$skill->id]) }}" enctype="multipart/form-data" />
		<input type="hidden" name="_method" value="PUT">
		{{ csrf_field() }}
		<div class="form-group">
			<label for="name">Skill</label>
			<input type="text" name="name" class="form-control" placeholder="Enter skill name" value="{{ $skill->name }}">
			<span class="errors" style="color:#FF0000">{{$errors->first('name')}}</span>
			<!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
		</div>
		<div class="form-group">
			<label for="type">Type</label>
			<select name="type" class="form-control">
				<option value="coding" @if($skill->type=='coding') selected @endif>coding</option>
				<option value="design" @if($skill->type=='design') selected @endif>design</option>
				<option value="qa" @if($skill->type=='qa') selected @endif>qa</option>
				<option value="docs" @if($skill->type=='docs') selected @endif>docs</option>
			</select>
			<span class="errors" style="color:#FF0000">{{$errors->first('type')}}</span>
			<!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
		</div>



		<div class="row top10">
			<div class="col-lg-4">
				<input type="submit" class="btn btn-primary" onclick="this.disabled=true;this.value='Submitted, please wait...';this.form.submit();" />
			</div>
		</div>
		
		</Form>

	</div>
</main>
@stop