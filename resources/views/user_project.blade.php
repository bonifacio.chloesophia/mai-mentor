@extends('template.rush')

@section('content')

<script
			  src="https://code.jquery.com/jquery-3.5.1.min.js"
			  integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
			  crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

<style>
.rating {
    display: flex;
    flex-direction: row-reverse;
    justify-content: left;
}

.rating>input {
    display: none
}

.rating>label {
    position: relative;
    width: 1em;
    font-size: 2vw;
    color: #FFD600;
    cursor: pointer
}

.rating>label::before {
    content: "\2605";
    position: absolute;
    opacity: 0
}

.rating>label:hover:before,
.rating>label:hover~label:before {
    opacity: 1 !important
}

.rating>input:checked~label:before {
    opacity: 1
}

.rating:hover>input:checked~label:before {
    opacity: 0.4
}

</style>
	<div class="row" >
		

		<div class="col-md-12">

		<div class="panel panel-default"  style="width: 80%;margin: auto;">
			
					
			<div class="panel-body">
				<div class="row">	
						<div class="col-md-3">
							@include('user_nav')
						</div>
						<div class="col-md-9">
							<div class="panel panel-default"  style="width: 100%; margin: auto;">
								<div class="panel-heading" style="padding: 3px 15px">
									<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
										<h1 class="h2">Project : {{$project->title}}</h1>

										<h3 class="h3">Status : 
										@if($project->status=='active') 
										<span class="badge badge-primary">Active</span>
										@elseif($project->status=='review') 
										<span class="badge badge-info">For Review</span>
										@elseif($project->status=='inactive') 
										<span class="badge badge-danger">Inactive</span>
										@elseif($project->status=='wip') 
										<span class="badge badge-info">WIP</span>
										@else
										<span class="badge badge-success">Completed</span>
										@endif
										</h3>
									</div>


								</div>
								<div class="panel-body">
									<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
										<h5>Type : {{$project->type}}</h5>
									</div>
									<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
										<h5>Description</h5>
									</div>
							<!-- 		<div class="row">
										<div class="col-md-12">
										{!!$project->description!!}
										</div>
									</div> -->

									<div class="row">	

										<div class="col-md-12">
											<ul class="nav nav-pills nav-fill" id="myTab" role="tablist">
											  <li class="nav-item">
											    <a class="nav-link active" id="team-tab" data-toggle="tab" href="#team" role="tab" aria-controls="home" aria-selected="true">Team</a>
											  </li>
											  <li class="nav-item">
											    <a class="nav-link" href="#milestone" id="milestone-tab" data-toggle="tab" href="#milestone" role="tab" aria-controls="home" aria-selected="true">Milestone</a>
											  </li>
											<!--   <li class="nav-item">
											    <a class="nav-link" href="#rating" id="rating-tab" data-toggle="tab" href="#rating" role="tab" aria-controls="home" aria-selected="true">Rating</a>
											  </li> -->
											</ul>
										</div>
									</div>
									
									<div class="tab-content" id="myTabContent">
  										<div class="tab-pane fade show active" id="team" role="tabpanel" aria-labelledby="team-tab">
  											<div class="row">
												<div class="col-md-6">
													<canvas class="my-4" id="skilltypeChart"></canvas>
												</div>
												 
												<div class="col-md-6">
													<canvas class="my-4" id="skillChart"></canvas>
												</div>
												 
												 @if($project->status!='review')									
													
														<div class="col-md-12">
														<table class="table">
														  <thead>
															<tr>
																<th scope="col">Name</th>
																<th scope="col">Skills</th>
																<!-- <th scope="col"></th> -->
																<th scope="col">Rate</th>
															</tr>
														  </thead>
														  <tbody>
														  @php
														  	$codingCount = 0;
														  	$designCount = 0;
														  	$qaCount = 0;
														  	$docsCount = 0;
														  @endphp
														  @foreach($pp as $x)

														  	<tr>
														  		<td>{{$x->user->first_name}} {{$x->user->last_name}}</td>
														  		<td>
														  		@foreach($x->user->userSkills as $w)
														  			@if($w->skill->type=='coding')
														  			@php
														  			$codingCount=$codingCount+1;
														  			@endphp
														  			<span class="badge badge-success">{{$w->skill->name}}</span>		  			
														  			@elseif($w->skill->type=='design')		  			
														  			@php
														  			$designCount=$designCount+1;
														  			@endphp
														  			<span class="badge badge-warning">{{$w->skill->name}}</span>
														  			@elseif($w->skill->type=='qa')		  			
														  			@php
														  			$qaCount=$qaCount+1;
														  			@endphp
														  			<span class="badge badge-primary">{{$w->skill->name}}</span>
														  			@else
														  			@php
														  			$docsCount=$docsCount+1;
														  			@endphp		  			
														  			<span class="badge badge-danger">{{$w->skill->name}}</span>
														  			@endif
														  			
														  		@endforeach

														  		</td>
														  		<td>

																<div class="rating"> 
																	<input @if($x->user->projectRating($project->id)==5) checked="true" @endif onclick="setVote({{$x->id}},5);" type="radio" name="rating{{$x->id}}" value="5" id="rating{{$x->id}}5"><label for="rating{{$x->id}}5">☆</label> 
																	<input @if($x->user->projectRating($project->id)==4) checked="true" @endif  onclick="setVote({{$x->id}},4);" type="radio" name="rating{{$x->id}}" value="4" id="rating{{$x->id}}4"><label for="rating{{$x->id}}4">☆</label>
																	<input @if($x->user->projectRating($project->id)==3) checked="true" @endif onclick="setVote({{$x->id}},3);" type="radio" name="rating{{$x->id}}" value="3" id="rating{{$x->id}}3"><label for="rating{{$x->id}}3">☆</label> 
																	<input @if($x->user->projectRating($project->id)==2) checked="true" @endif onclick="setVote({{$x->id}},2);" type="radio" name="rating{{$x->id}}" value="2" id="rating{{$x->id}}2"><label for="rating{{$x->id}}2">☆</label> 
																	<input  @if($x->user->projectRating($project->id)==1) checked="true" @endif onclick="setVote({{$x->id}},1);" type="radio" name="rating{{$x->id}}" value="1" id="rating{{$x->id}}1"><label for="rating{{$x->id}}1">☆</label>
																</div>
															  		<!-- <div class="btn-group" role="group" aria-label="">
																	
																		<a href="{{ route('projects.professionals.delete',[$project->id,$x->user->id]) }}"><button type="button" class="btn btn-danger">Remove</button></a>
																	</div> -->
																</td>
														  	</tr>
														  @endforeach

															
														  </tbody>
														</table>
														</div>
												
													@endif

											<!-- 	<a href="{{ route('projects.create') }}" role="button" class="btn btn-primary">Assign Member</a> -->

											</div>

  										</div>
										<div class="tab-pane fade" id="milestone" role="tabpanel" aria-labelledby="milestone-tab">
											
											<div class="row">
												<div class="col-md-12">
													<table class="table">
														<tr>
															<th>Milestone</th>
															<th>Date Start</th>
															<th>Date End</th>
															<th>Professional</th>
															<th>Dev status</th>
															<th>Milestone Status</th>
														</tr>
														@foreach($pm as $x)
															<tr>
																<td>{{$x->name}}</td>
																<td>{{$x->start}}</td>
																<td>
																@if(Carbon\Carbon::parse($x->end)->year>1)
																{{$x->end}}
																@endif
																 </td>
															
																@if($x->professionals)
																	<td>{{$x->professionals->user->first_name}} {{$x->professionals->user->last_name}}</td>
																@else
																	<td></td>
																@endif
																<td>
																	@if($x->done==1)
																		done
																	@else
																		not done
																	@endif
																</td>
																<td>
																	@if(Carbon\Carbon::parse($x->end)->year > 1)
																		@if(Carbon\Carbon::parse($x->end)->diffInDays(Carbon\Carbon::now()) <= 0 && $x->done==1)
																		done
																		@elseif(Carbon\Carbon::parse($x->end)->diffInDays(Carbon\Carbon::now()) <= 0 && $x->done!=1)
																		overdue
																		@else
																		<!-- {{Carbon\Carbon::parse($x->end)->diffInDays(Carbon\Carbon::now())}} -->
																		pending
																		@endif
																	@else
																	done
																	@endif
																	
																</td>
																
															</tr>
														@endforeach
													</table>
												</div>
											</div>
										</div>
										<div class="tab-pane fade" id="rating" role="tabpanel" aria-labelledby="rating-tab">...</div>
									</div>
									</div>
									
								
							</div>
						</div>
				</div>

			</div>
		</div>

		</div>
	</div>
	</br>
	<link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>


<script>
	$(document).ready(function() {



	});

</script>


<script src="{{ URL::asset('js/chart.js') }}"></script>
@if($project->status!='review')
<script>


// $(".pp_rating").on('click', function(event){

//     // event.stopPropagation();
//     // event.stopImmediatePropagation();
//     //(... rest of your JS code)
// });

setVote = function (id,rating)
  {

    
	  $.get("/project/" + {{$project->id}} + "/professional/"+ id +"/rating/"+rating, function(data, status){
	    // alert("Data: " + data + "\nStatus: " + status);
	  });

  }


var ctx = document.getElementById("skilltypeChart").getContext('2d');
var myChart = new Chart(ctx, {
	type: 'pie',	
	data: {
		datasets: [{
		data: [{{$codingCount}}, {{$designCount}}, {{$qaCount}},{{$docsCount}}],        
		// data: [1, 2, 3,4],        
		 backgroundColor:  [
		 "rgba(0, 255, 0, 0.5)",
		 "rgba(255, 159, 64, 0.5)",
		 "rgba(0, 0, 255, 0.5)",
		 "rgba(255, 0, 0, 0.5)",
		 "rgba(54, 162, 235, 0.5)",
		 "rgba(153, 102, 255, 0.5)",
		 "rgba(201, 203, 207, 0.5)"],
		borderColor:  [
		"rgba(0, 255, 0, 1)",
		"rgba(255, 159, 64, 1)",
		"rgba(0, 0, 255, 1)",
		"rgba(255, 0, 0, 1)",
		"rgba(54, 162, 235, 1)",
		"rgba(153, 102, 255, 1)",
		"rgba(201, 203, 207, 1)"]
	}],
	labels: ['coding','design','qa','docs' ],   
   
	},
	 options: {
		tooltips: {
			mode: 'index',
			intersect: false
		},
		legend: {
    		position: 'bottom'
 		},
	}
});


var ctx2 = document.getElementById("skillChart").getContext('2d');
var myChart2 = new Chart(ctx2, {
	type: 'bar',	
	data: {
		datasets: [{
			label: 'Skill Sets',
		data: [

			@foreach($skills as $x)

				{{$skills_collection->where('name',$x->name)->count() }},
				
			@endforeach

		],        
		  backgroundColor: [
        'rgba(255, 99, 132, 0.2)',
        'rgba(54, 162, 235, 0.2)',
        'rgba(255, 206, 86, 0.2)',
        'rgba(75, 192, 192, 0.2)',
        'rgba(153, 102, 255, 0.2)',
        'rgba(255, 159, 64, 0.2)',
        'rgba(255, 99, 132, 0.2)',
        'rgba(54, 162, 235, 0.2)',
        'rgba(255, 206, 86, 0.2)',
        'rgba(75, 192, 192, 0.2)',
        'rgba(153, 102, 255, 0.2)',
        'rgba(255, 159, 64, 0.2)'
      ],
      borderColor: [
        'rgba(255,99,132,1)',
        'rgba(54, 162, 235, 1)',
        'rgba(255, 206, 86, 1)',
        'rgba(75, 192, 192, 1)',
        'rgba(153, 102, 255, 1)',
        'rgba(255, 159, 64, 1)',
        'rgba(255,99,132,1)',
        'rgba(54, 162, 235, 1)',
        'rgba(255, 206, 86, 1)',
        'rgba(75, 192, 192, 1)',
        'rgba(153, 102, 255, 1)',
        'rgba(255, 159, 64, 1)'
      ],
      borderWidth: 1
	}],
	labels: [
	@foreach($skills as $x)
		'{{$x->name}}',
	@endforeach
	],   
   
	},
	 options: {
	 	scales: {
		        yAxes: [{
		            ticks: {
		                beginAtZero: true
		            }
		        }]
		    },
		tooltips: {
			mode: 'index',
			intersect: false
		},
		legend: {
    		position: 'bottom'
 		},
	}
});

</script>

@endif


@stop