@extends('template.rush')

@section('content')


	<div class="row" >
		

		<div class="col-md-12">

		<div class="panel panel-default"  style="width: 80%;margin: auto;">
			
					
			<div class="panel-body">
				<div class="row">	
						<div class="col-md-3">
							@include('user_nav')
						</div>
						<div class="col-md-9">
							<div class="panel panel-default"  style="width: 100%; margin: auto;">
								<div class="panel-heading" style="padding: 3px 15px">
									<h5>My Projects</h5>			
								</div>
								<div class="panel-body">
									<table class="table">
										<tr>
											<th>Title</th>
											<th>Type</th>
											<th>Status</th>
											<th></th>
										</tr>
										@foreach($project as $x)
										<tr>
											<td>{{$x->title}}</td>
											<td>{{$x->type}}</td>
											<td>
												@if($x->status=='active') 
												<span style="height: 10px; width: 10px;  background-color: #00ff00;  border-radius: 50%;  display: inline-block;"></span> Active
												@elseif($x->status=='review') 
												<span style="height: 10px; width: 10px;  background-color: #5bc0de;  border-radius: 50%;  display: inline-block;"></span> For Review
												@elseif($x->status=='inactive') 
												<span style="height: 10px; width: 10px;  background-color: #FF0000;  border-radius: 50%;  display: inline-block;"></span> Inactive
												@elseif($x->status=='wip') 
												<span style="height: 10px; width: 10px;  background-color: #FF0000;  border-radius: 50%;  display: inline-block;"></span> WIP
												@else
												<span style="height: 10px; width: 10px;  background-color: #00FF00;  border-radius: 50%;  display: inline-block;"></span> completed
												@endif
											</td>
											
											<td>
												<a href="{{ route('user-project',[$x->id])}}"><button type="button" class="btn btn-danger">View</button></a>
											</td>
										</tr>
										@endforeach
									</table>
									<div style="text-align: center">
										{{ $project->appends(
											[
												
											]

											)->links() }}
									</div>
								
								</div>
							</div>
						</div>
				</div>

			</div>
		</div>

		</div>
	</div>



@stop