@extends('template.rush')

@section('content')
<script src="https://www.google.com/recaptcha/api.js" async defer></script>


<div class="row p-lg-12">
	<div class="col-md-8">
		<img src="{{ URL::asset('images/programming_option.png')}}" class="img-fluid" alt="Responsive image">
	</div>
	<div class="col-md-4" >
		<div class="col-auto mb-5">
			<form action="{{ route('development-submit',[]) }}" method="POST" >
				{{ csrf_field() }}
				@if($type=='web')
				<h3>Web Project Details</h3>
				@elseif($type=='mobile')
				<h3>Mobile Project Details</h3>
				@else
				<h3>Software Project Details</h3>
				@endif
				<div class="form-group">
					<label for="title">Project Title</label>
					<input type="text" name="title" class="form-control">
				</div>

				<input type="hidden" name="type" value="{{ $type }}">
				<div class="form-group">
					
					<p>Provide a detailed description on what you want your project to look like. Add all needed specifications and features.</p>
					<TEXTAREA class="form-control" name="description"></TEXTAREA>
				</div>
				<div class="form-group">
					<p>If there are any additional files (ex. drafts, pictures, files) upload those files to be included in the website</p>
					<span class="btn btn-default btn-file">
						<input id="input-2" name="input2[]" type="file" class="file" multiple data-show-upload="true" data-show-caption="true">
					</span>
				</div>
				<div class="g-recaptcha" data-sitekey="6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI"></div>
				<button type="submit" class="btn btn-primary">Submit</button>
			</form>
		</div>
	</div>
</div>
@stop