@extends('template.rush')

@section('content')

<div class="row bg-light mx-5 d-flex">

	<div class="col-md-4">
	
	</div>
	<div class="col-md-4">
		<div class="d-flex justify-content-center">
			<div class="align-self-center top_div_text">Login to your Account</div>
		</div>
		@if(Session::has('flash_message'))
			<div class="alert alert-success alert-dismissible">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			{{Session::get('flash_message')}}
			</div>
		@endif

		@if(Session::has('flash_error'))
			<div class="alert alert-danger">{{Session::get('flash_error')}}</div>
		@endif
		<br/>
		<form action="{{ route('login',[]) }}" method="POST" >
			{{ csrf_field() }}		
			<div class="form-group">				
				<label for="email">Email</label>
				<input type="email" name="email" class="form-control">
				{{$errors->first('email')}}</span>
			</div>
			<div class="form-group">				
				<label for="password">Password</label>
				<input type="password" name="password" class="form-control">
				{{$errors->first('password')}}</span>
			</div>
			<br/>
			<button type="submit" class="btn btn-primary">Submit</button>
		<br/>
		</form>

	</div>
	<div class="col-md-4">
	
	</div>
</div>
<div class="row">
	<div class="col-md-4">
		&nbsp;
	</div>
</div>

@stop