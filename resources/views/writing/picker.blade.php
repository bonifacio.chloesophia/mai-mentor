@extends('template.rush')

@section('content')

<div class="row p-lg-12">
	
	<div class="col-md-4" >
		<div class="card-deck text-center justify-content-center ">
			<div class="col-auto mb-5 my-auto">
				<div class="card" style="height: 15rem">
					<a href="{{ route('writing-picker') }}">						
						<div class="card-body">	
							<div class="row">
								<div class="col-md-12">
									<a href="{{ URL::route('writing-form',['type'=>'content_writing']) }}" class="btn btn-danger btn-lg active" role="button" aria-pressed="true">Content Writing</a>	
									<br/>
									<br/>
								</div>
								<div class="col-md-12">
									<a href="{{ URL::route('writing-form',['type'=>'creative_writing']) }}" class="btn btn-danger btn-lg active" role="button" aria-pressed="true">Creative writing</a>	
									<br/>
									<br/>
								</div>
								<div class="col-md-12">
									<a href="{{ URL::route('writing-form',['type'=>'proofread']) }}" class="btn btn-danger btn-lg active" role="button" aria-pressed="true">Editing & Proofreading</a>	
									<br/>
									<br/>
								</div>
							</div>
								
						</div>
					</a>
				</div>
			</div>
		</div>
		
	</div>
	<div class="col-md-8">
		<img src="{{ URL::asset('images/writing_option.png')}}" class="img-fluid" alt="Responsive image">
	</div>
</div>
@stop