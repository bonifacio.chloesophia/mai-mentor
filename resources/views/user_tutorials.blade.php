@extends('template.rush')

@section('content')


	<div class="row" >
		

		<div class="col-md-12">

		<div class="panel panel-default"  style="width: 80%;margin: auto;">
			
					
			<div class="panel-body">
				<div class="row">	
						<div class="col-md-3">
							@include('user_nav')
						</div>
						<div class="col-md-9">
							<div class="panel panel-default"  style="width: 100%; margin: auto;">
								<div class="panel-heading" style="padding: 3px 15px">
									<h5>My Projects</h5>			
								</div>
								<div class="panel-body">
									<table class="table">
										<tr>
											<th>Subject</th>
											<th>Date and Time</th>
											<th>Hours</th>
											<th>Status</th>
											<th></th>
										</tr>
										@foreach($tutorials as $x)
										<tr>
											<td>{{$x->subject}}</td>
											<td>{{$x->date_of_tutorial}}</td>
											<td>{{$x->hours}}</td>
											<td>{{$x->status}}</td>
											<!-- <td>{{$x->details}}</td> -->
										
											
											<td>
												<a href="{{ route('user-project',[$x->id])}}"><button type="button" class="btn btn-danger">View</button></a>
											</td>
										</tr>
										@endforeach
									</table>
									<div style="text-align: center">
										{{ $tutorials->appends(
											[
												
											]

											)->links() }}
									</div>
								
								</div>
							</div>
						</div>
				</div>

			</div>
		</div>

		</div>
	</div>



@stop