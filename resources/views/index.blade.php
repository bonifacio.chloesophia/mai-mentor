@extends('template.rush')

@section('content')

<div class="row p-lg-5">
		<div class="col-md-6">
			<img src="{{ URL::asset('images/main_option.png')}}" class="img-fluid" alt="Responsive image">
		</div>
		<div class="col-md-6">
			<br/>
			<br/>
			<br/>
			<h2>BEAT THE</h2>
			<h1>DEADLINE</h1>
			<p style="color:#ff0000">got any projects you havent finshed yet?</p>
			<p>mAI mentor provides network of Filipino talents and volunteers  who pledge themselves to provide access to mentoring to those in need of support in this difficult times through our mAi mentor online community.</p>
			<br/>
			<br/>
			<br/>
			<a href="{{ URL::route('get-started',[]) }}" class="btn btn-danger btn-lg active" role="button" aria-pressed="true">GET STARTED</a>
		</div>
	</div>

	<div class="row p-lg-3 mx-5">
		<div class="col-md-12" >
			<h2 class="text-center">Services Offered</h2>
			<p class="text-center">WHAT DO YOU NEED TO FINISH?</p>
		</div>
		<div class="card-deck">
	
			<div class="card" style="width:18rem">
				<img class="card-img-top card-main p-5" src="{{ URL::asset('images/dev.png')}}" alt="Card image">
			  	<div class="card-body">
			    	<h4 class="card-title">DEVELOPMENT</h4>
			    	<p class="card-text">Some example text.</p>
			    	<a href="{{ route('development-picker') }}" class="btn btn-primary">See Profile</a>
			  	</div>
			</div>

			<div class="card" style="width:18rem">
  				<img class="card-img-top card-main p-5" src="{{ URL::asset('images/design.png')}}" alt="Card image">
			  	<div class="card-body">
			    	<h4 class="card-title">CREATIVES</h4>
			    	<p class="card-text">Some example text.</p>
			    	<a href="{{ route('creatives-picker') }}" class="btn btn-primary">See Profile</a>
			  	</div>
			</div>

			<div class="card" style="width:18rem">
  				<img class="card-img-top card-main p-5" src="{{ URL::asset('images/writing.png')}}" alt="Card image">
			  	<div class="card-body">
			    	<h4 class="card-title">WRITING</h4>
			    	<p class="card-text">Some example text.</p>
			    	<a href="{{ route('writing-picker') }}" class="btn btn-primary">See Profile</a>
			  	</div>
			</div>
			
			<div class="card" style="width:18rem">
  				<img class="card-img-top card-main p-5" src="{{ URL::asset('images/papers.png')}}" alt="Card image">
			  	<div class="card-body">
			    	<h4 class="card-title">PAPERS</h4>
			    	<p class="card-text">Some example text.</p>
			    	<a href="{{ route('paper-picker') }}" class="btn btn-primary">See Profile</a>
			  	</div>
			</div>

			<div class="card" style="width:18rem">
  				<img class="card-img-top card-main p-5" src="{{ URL::asset('images/printer.png')}}" alt="Card image">
			  	<div class="card-body">
			    	<h4 class="card-title">PRINTING</h4>
			    	<p class="card-text">Some example text.</p>
			    	<a href="{{ route('print-form') }}" class="btn btn-primary">See Profile</a>
			  	</div>
			</div>
			
		</div>
	</div>

	<div class="row bg-light mx-5 d-flex">
		<div class="col-md-12">
			<div class="d-flex justify-content-center">
				<div class="align-self-center top_div_text">We Also Offer..</div>
			</div>
		</div>
		<div class="col-md-12">
			<div class="row justify-content-center">
				<div class="col-md-3 text-center">
					<div class="circle">
						<img src="{{ URL::asset('images/teaching.png')}}" class="img-fluid center_icon"> 
		            </div>
				</div>
				<div class="col-md-3">
					<h2>Tutorials</h2>
					<p>Struggling with you lessons?</p>
					<p>mAI mentor also offers tutorial services for
					those struggling with their subjects</p>
					<button class="btn btn-sm btn-danger">tutor me</button>
				</div>
				<div class="col-md-3 text-right">
					<h2 class="text-right">Workshops</h2>
					<p>What to learn with your peers?</p>
					<p>mAI mentor also offers workshops to acquire special skills
					and knowledge</p>
					<button class="btn btn-sm btn-danger">book me</button>
				</div>

				<div class="col-md-3 text-center">
					<div class="circle">
						<img src="{{ URL::asset('images/workshop.png')}}" class="img-fluid center_icon"> 
		            </div>
				</div>
			</div>
		</div>
	</div>

	<div class="row bg-light mx-5 my-5 d-flex">
		<div class="col-md-12">
			<div class="d-flex justify-content-center">
				<div class="align-self-center top_div_text">Our Experts</div>
			</div>
		</div>

		<div class="col-md-12 my-5">
			<div class="card-deck">
				
				@foreach($dev_view_share as $x)
					<div class="card" style="width:18rem">
						<img class="card-img-top card-main" src="{{ URL::asset('images/anon.png')}}" alt="Card image">
					  	<div class="card-body">
					    	<h5 class="card-title">{{ $x->user->first_name }} {{ $x->user->last_name }}</h5>
					    	<p class="card-text small">Programming | Design | Creatives</p>
					    	Rating 
					    	
							<span class="fas fa-star @if($x->rating() >=1) checked @endif"></span>
							<span class="fas fa-star @if($x->rating() >=2) checked @endif"></span>
							<span class="fas fa-star @if($x->rating() >=3) checked @endif"></span>
							<span class="fas fa-star @if($x->rating() >=4) checked @endif"></span>
							<span class="fas fa-star @if($x->rating() >=5) checked @endif"></span>
					    	
					  	</div>
					</div>

				@endforeach
				

			</div>
		</div>
	</div>

	<div class="row mx-5 my-5 bg-light d-flex">
		<div class="col-md-12">
			<div class="d-flex justify-content-center">
				<div class="align-self-center top_div_text">How it Works</div>
			</div>
		</div>
		<div class="col-md-12 d-flex justify-content-center">
			<div class="row my-5">
				<div class="col-md-4">
					<div class="card" style="width: 18rem;">
						<img src="{{ URL::asset('images/gmail.png')}}" class="img-fluid card-img-top card-main"> 
						<div class="card-body">
						<h5 class="card-title">Choose a category</h5>
						<p class="card-text">Tell us about your project by choosing a category that best suits your needs. mAI mentor offers a wide range of fields to help its customers.</p>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="card" style="width: 18rem;">
						<img src="{{ URL::asset('images/gmail.png')}}" class="img-fluid card-img-top card-main"> 
						<div class="card-body">
						<h5 class="card-title">Collaborate</h5>
						<p class="card-text">Contact the expert working on your project with ease with their provided contact information. Collaborate with your expert and monitor all the progress and changes.</p>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="card" style="width: 18rem;">
						<img src="{{ URL::asset('images/gmail.png')}}" class="img-fluid card-img-top card-main"> 
						<div class="card-body">
						<h5 class="card-title">Pay on pick-up</h5>
						<p class="card-text">Pick-up your project on the nearest branch available to you and pay at the same time. Your project will be ready for submission. </p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


	<div class="row mx-5 my-5 p-5 bg-light d-flex">
		
		<div class="col-md-6 ">
			<h1>Be A part of our team</h1>
			<p>We want you to join our team of experts to help us provide rush services to students</p>
			<p>Simply apply and provide your credentials, portfolio and contact information. Our team will get back to you. It's easy!</p>
		</div>

		<div class="col-md-6 d-flex justify-content-center">
			<div class="row align-items-center">
				<div class="col-md-12">
					<button class="btn btn-danger">APPLY NOW!</button>
				</div>
			</div>
		</div>

	</div>



@stop