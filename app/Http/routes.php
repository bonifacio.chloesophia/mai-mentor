<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'PageController@index')->name('index');

Route::get('/register', 'PageController@register')->name('register');
Route::get('/login', 'PageController@login')->name('login');
Route::get('/logout', 'UserController@logout')->name('logout');
Route::post('/login', 'UserController@authenticate')->name('login-post');
Route::post('/users', 'UserController@store')->name('register-submit');


Route::get('/get-started', 'PageController@getStarted')->name('get-started');
Route::get('/webmail', 'PageController@webmail')->name('web-mail');



Route::group(['middleware' => ['auth']], function() {

	Route::get('/project/{id}/professional/{pro_id}/rating/{rating}','ProjectsController@rating')->name('project-rating');

	Route::get('/dashboard', 'PageController@dashboard')->name('dashboard');

	Route::get('/services', 'PageController@services')->name('services');

	Route::get('/tutor', 'PageController@tutorForm')->name('tutor-form');	
	Route::post('/tutor', 'PageController@tutorSubmit')->name('tutor-submit');

	Route::get('/development', 'PageController@developmentPicker')->name('development-picker');	
	Route::get('/development-form/{type}', 'PageController@developmentForm')->name('development-form');
	Route::post('/development', 'PageController@formSubmit')->name('development-submit');

	Route::get('/creatives', 'PageController@creativesPicker')->name('creatives-picker');	
	Route::get('/creatives-form/{type}', 'PageController@creativesForm')->name('creatives-form');
	Route::post('/creatives', 'PageController@formSubmit')->name('creatives-submit');

	Route::get('/writing', 'PageController@writingPicker')->name('writing-picker');	
	Route::get('/writing-form/{type}', 'PageController@writingForm')->name('writing-form');
	Route::post('/writing', 'PageController@formSubmit')->name('writing-submit');

	Route::get('/paper', 'PageController@paperPicker')->name('paper-picker');	
	Route::get('/paper-form/{type}', 'PageController@paperForm')->name('paper-form');
	Route::post('/paper', 'PageController@formSubmit')->name('paper-submit');

	Route::get('/profile','UserController@profile')->name('profile');
	Route::post('/profile-update','UserController@profileUpdate')->name('profile-update');
	Route::get('/user-project/{id}','UserController@userProject')->name('user-project');
	Route::get('/user-projects','UserController@userProjects')->name('user-projects');

	Route::get('/user-tutorials/{id}','UserController@userTutorial')->name('user-tutorial');
	Route::get('/user-tutorials','UserController@userTutorials')->name('user-tutorials');

	Route::get('/print', 'PageController@printForm')->name('print-form');	
	Route::post('/print', 'PageController@printSubmit')->name('print-submit');


	Route::group(['middleware' => 'permission:is_allow_skills'], function() {
		Route::resource('skills', 'SkillsController');
	});

	Route::group(['middleware' => 'permission:is_allow_professionals'], function() {
		Route::resource('professionals', 'ProfessionalsController');
		Route::resource('professionals.skills','ProfessionalSkillsController');
	});

	Route::group(['middleware' => 'permission:is_allow_clients'], function() {
		Route::resource('clients', 'ClientController');
		// Route::resource('professionals.skills','ProfessionalSkillsController');
	});

	Route::group(['middleware' => 'permission:is_allow_projects'], function() {
		Route::resource('projects', 'ProjectsController');

		Route::get('project/{project_id}/pro-delete/{id}','ProjectProfessionalsController@deletePro')->name('projects.professionals.delete');

		Route::resource('projects.professionals','ProjectProfessionalsController');

		Route::get('project/{project_id}/milestone-delete/{id}','ProjectMilestonesController@deleteMilestone')->name('projects.milestones.delete');
		Route::get('project/{project_id}/milestone-done/{id}','ProjectMilestonesController@doneMilestone')->name('projects.milestones.done');

		Route::resource('projects.milestones','ProjectMilestonesController');
	});




// 		//changepassword
// 		Route::get('/editprofile','AdminController@editprofile')->name('admin.show.profile');
// 		Route::post('/updateprofile','AdminController@updateProfile')->name('admin.update.profile');
// 		Route::get('/changepassword','AdminController@showChangePassword')->name('admin.show.change.password');
// 		Route::post('/changepassword','AdminController@updatePassword')->name('admin.update.password');
// 		Route::get('/help','SettingsController@help')->name('esaklolo.help.help');

// 		Route::group(['middleware' => 'permission:is_allow_roles'], function() {
// 			Route::resource('roles', 'RolesController');
// 			Route::get('roles/{id}/old_privilege', 'RolesController@oldPrivilege')->name('roles-privilege');
// 			Route::get('roles/{id}/privilege', 'RolesController@privilege')->name('roles-privilege');
// 			Route::post('roles/{id}/update/{x}', 'RolesController@updatePrivilege')->name('roles-update-privilege');
// 			Route::get('role-delete/{id}', 'RolesController@destroy')->name('roles-delete');
// 			// Route::post('/role/update','RolesController@webroleupdate')->name('esaklolo.web_user.update');
			
// 		});
	Route::group(['middleware' => 'permission:is_allow_settings'], function() {
		Route::resource('settings', 'SettingsController');
	});
});
