<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Project;
use App\User;
use App\Rating;
use Log;
use Response;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Validator;
use Session;
class ProjectsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function rating($project_id,$pp_id,$rating)
    {
        
        Rating::where('project_id','=',$project_id)
            ->where('professional_id','=',$pp_id)
            ->delete();
        Rating::create([
            'project_id'=>$project_id,
            'professional_id'=>$pp_id,
            'rating'=>$rating,
            ]);
       return Response::json(['success'=>true],200, array(),JSON_PRETTY_PRINT);
    }
    public function index()
    {
        //
        $projects = Project::with('user')->paginate(10);

        return view('projects.index',[
            'projects'=>$projects,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $clients = User::get();
        return view('projects.create',[
            'clients'=>$clients,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // Log::info($request);
        $validator = Validator::make($request->all(), [
            'title' => 'required|min:2|max:50',
            'user_id' => 'required',
            'description' => 'required',
         
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }

        $user = Project::create([
            'title'  =>  $request->input('title'),
            'user_id' => $request->input('user_id'),
            'description' => $request->input('description'),
        ]);

        return redirect()->route('projects.index')->with('flash_message', 'Projects Created');
     
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $projects = Project::with('user')->find($id);
        $clients = User::get();
        return view('projects.edit',[
            'projects'=>$projects,
            'clients'=>$clients,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        // Log::info($request);

        $validator = Validator::make($request->all(), [
            'title' => 'required|min:2|max:50',
            'user_id' => 'required',
            'description' => 'required',
         
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }
        $project = Project::find($id);
        $project->title = $request->input('title');
        $project->user_id = $request->input('user_id');
        $project->status = $request->input('status');
        $project->description = $request->input('description');
        $project->save();

      
        return redirect()->route('projects.index')->with('flash_message', 'Projects Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
