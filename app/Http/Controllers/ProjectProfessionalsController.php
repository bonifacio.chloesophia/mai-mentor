<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Project;
use App\Skills;
use App\Professionals;
use App\ProjectProfessional;
use Log;
class ProjectProfessionalsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($project_id)
    {
        //
        $project = Project::with('user')->find($project_id);
        $professionals = Professionals::with('user')->get();
        $pp = ProjectProfessional::with('user')->where('project_id','=',$project_id)->get();

        // Log::info($pp);
        $skills = Skills::get();
        $skillsCollection = collect();
        foreach($pp as $x) 
        {
            foreach($x->user->userSkills as $e) 
            {
                $g = Skills::find($e->skill_id);
                // if(!$c->contains($g))
                // {
                    $skillsCollection->push($g);
                // }
            }
          
        }

        return view('projects.professionals.index',[
            'project'=>$project,
            'professionals'=>$professionals,
            'pp'=>$pp,
            'skills_collection'=>$skillsCollection,
            'skills'=>$skills,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($project_id)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$project_id)
    {
        //
        // Log::info($request);
        // Log::info($project_id);
        $project = Project::find($project_id);
        $project->status = 'wip';
        $project->save();

        $pp = ProjectProfessional::firstOrCreate([
            'project_id'=>$project_id,
            'user_id'=>$request->user_id,
            ]);

        return redirect()->route('projects.professionals.index',$project_id)->with('flash_message', 'Pro added to Team');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id,$project_id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id,$project_id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id,$project_id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,$project_id)
    {
        //
    }
    public function deletePro($project_id,$id)
    {
        
        ProjectProfessional::where('project_id',$project_id)->where('user_id',$id)->delete();
        return redirect()->back()->with('flash_message', 'Pro Removed to Team');
    }
}
