<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Skills;
use App\Professionals;
use App\User;
use App\UserSkills;
use Log;
class ProfessionalSkillsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($professional_id)
    {
        //
        $professional = Professionals::with('user')->find($professional_id);
        $user = User::find($professional->user_id);
        $skills = Skills::get();
        $user_skills = UserSkills::where('user_id',$professional->user_id)->get();
        // Log::info($user_skills);
        return view('professionals.skills',[
            'user'=>$user,
            'professional'=>$professional,
            'skills'=>$skills,
            'user_skills'=>$user_skills,

        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($professional_id)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($professional_id,Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($professional_id,$id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($professional_id,$id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($professional_id,Request $request, $id)
    {
        //
      
        $professional = Professionals::with('user')->find($professional_id);
        $data = $request->except(['_token','_method']);


        UserSkills::where('user_id',$professional->user_id)->delete();


        foreach($data as $key => $value)
        {
            
            UserSkills::firstOrCreate([
                'user_id'=>$professional->user_id,
                'skill_id'=>$key,
            ]);
        }

        return redirect()->route('professionals.index')->with('flash_message', 'Good!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($professional_id,$id)
    {
        //
    }
}
