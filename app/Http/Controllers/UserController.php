<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Log;
use Hash;
use Validator;
use Session;
use App\User;
use App\Client;
use App\Professionals;
use App\ProjectProfessional;
use App\ProjectMilestone;
use App\Tutorial;
use App\Privileges;
use App\Skills;
use App\Project;
use App\Variable;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
class UserController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		//
	}

	public function logout()
	{
		//
		if(Auth::check())
		{
		
			auth()->logout();

		}		
				
		return redirect('/')->with('flash_message', 'Logged out!');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	public function profile()
	{
		//
		$user = User::find(Auth::user()->id);
		// Log::info($user);
		return view('profile',[
			'user'=>$user,
		]);
	}

	public function userTutorials()
	{
		$tutorials = Tutorial::orderBy('id','desc')->where('user_id','=',Auth::user()->id)->paginate(10);

		return view('user_tutorials',[
			'tutorials'=>$tutorials,
		]);
	}

	public function userProjects()
	{
		$project = Project::orderBy('id','desc')->where('user_id','=',Auth::user()->id)->paginate(10);

		return view('user_projects',[
			'project'=>$project,
		]);
	}

	public function userProject($id)
	{

		$project = Project::find($id);
		$professionals = Professionals::with('user')->get();
		$pp = ProjectProfessional::with('user')->where('project_id','=',$project->id)->get();

		$skills = Skills::get();
        $skillsCollection = collect();
        foreach($pp as $x) 
        {
            foreach($x->user->userSkills as $e) 
            {
                $g = Skills::find($e->skill_id);
                // if(!$c->contains($g))
                // {
                    $skillsCollection->push($g);
                // }
            }
          
        }

       	$pm = ProjectMilestone::with('professionals')->where('project_id','=',$project->id)->get();


		return view('user_project',[
			'project'=>$project,
			'professionals'=>$professionals,
			'pp'=>$pp,
			'pm'=>$pm,
            'skills_collection'=>$skillsCollection,
            'skills'=>$skills,
		]);
	}


	public function profileUpdate(Request $request)
	{
		// Log::info($request);

		$validator = Validator::make($request->all(), [
			'first_name' => 'required|min:2|max:50',
			'last_name' => 'required|min:2|max:50',
			'email' => 'required|email|unique:users,email,'.Auth::user()->id,
			'contact_no' => 'required|min:2|max:255',
			
		 
		]);

		if ($validator->fails()) {
			return redirect()
				->route('profile')
						->withErrors($validator)
						->withInput();
		}

		//
		$user = User::find(Auth::user()->id);
		$user->first_name = $request->input('first_name');
		$user->last_name = $request->input('last_name');
		$user->email = $request->input('email');
		$user->contact_no = $request->input('contact_no');
		$user->save();

		return redirect('profile')->with('flash_message', 'Updated Profile');
	}

	public function authenticate()
	{

		$credentials = [
			'email' => Input::get('email'),
			'password' => Input::get('password'),
		];
		if(!Auth::attempt($credentials))
		{
			auth()->logout();
			Session::flash('flash_error','Wrong username/password!');

			// DB::table('activity_log')->insert([
			// 'username'  =>  $request->input('username').'@'.\Request::ip(),
			// 'entry'  =>  'login-failure',
			// 'comment'  =>  '',
			// 'family'  =>  'login-failure',
			// 'created_at' => \Carbon\Carbon::now()
			// ]);

			return redirect()->back();
		}

	
		$this->checkIfExist('is_allow_settings');
		$this->checkIfExist('is_allow_activity_logs');
		$this->checkIfExist('is_allow_roles');
		$this->checkIfExist('is_allow_role_privilege');
		$this->checkIfExistVar('software_name','','Software Name');
		$this->checkIfExistVar('company_name','','Company Name');
		$this->checkIfExistVar('company_address','','Company Address');
		$this->checkIfExistVar('company_phone','','Company Phone');
		$this->checkIfExistVar('software_description','','Software Description');
		$this->checkIfExistVar('company_logo','','Company Logo');
		$this->checkIfExistVar('report_footer','','Report Footer');

		
		$privileges = Privileges::get();
		foreach ($privileges as $privilege) {
			$privilegeText = ''.$privilege->name;
			$this->checkIfExist($privilegeText);
			$p = User::find(Auth::user()->id)->checkPrivileges($privilegeText);
			Session([$privilegeText =>  $p]); 
		}

		$var = Variable::all();
		foreach ($var as $x) {
			Session([$x->name=>$x->value]); 

		}
		$user = Auth::user();
		
		Session::flash('flash_message','Logged in!');
		
		if($user->isRole('superuser'))
		{
			return redirect()
				->route('dashboard')
				->with('flash_message', 'Logged in!');
		}
		else
		{
			return redirect('/')->with('flash_message', 'Logged in!');
		}
		// DB::table('activity_log')->insert([
		// 	'username'  =>  Auth::user()->username.'@'.\Request::ip(),
		// 	'entry'  => 'login-success',
		// 	'comment'  =>  '',
		// 	'family'  =>  'login-success',
		// 	'created_at' => \Carbon\Carbon::now()
		// 	]);

		//if(Auth::user()->status=='customer')
		//{
			
		// }
		// else
		// {
		// 	return redirect('home');
		// }
	}

	public function validateCaptcha($response)
	{
		if(isset($response) && !empty($response))
		{
			$secret = '6LeIxAcTAAAAAGG-vFI1TnRWxMZNFuojJ4WifJWe';
			$verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
			$responseData = json_decode($verifyResponse);

			if($responseData->success)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

	}
	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		//
	
		if(!$this->validateCaptcha($_POST['g-recaptcha-response']))
		{
			redirect()->route('register')->with('flash_error', 'Invalid recaptcha');
		}

		$validator = Validator::make($request->all(), [
			'first_name' => 'required|min:2|max:50',
			'last_name' => 'required|min:2|max:50',
			'email' => 'required|email|unique:users,email',
			'contact_no' => 'required|min:2|max:255',
			'password' => 'required|min:1|max:50',
			'password_confirmation' => 'required|same:password',
		 
		]);

		if ($validator->fails()) {
			return redirect()
				->route('register')
						->withErrors($validator)
						->withInput();
		}
	 
		
		$user = User::create([
			'first_name'  =>  $request->input('first_name'),
			'last_name' => $request->input('last_name'),
			'email' => $request->input('email'),
			'contact_no' => $request->input('contact_no'),
			'password' => Hash::make($request->input('password')),
			'type' => $request->input('type'),

		]);

		if($request->input('type')=='student' || $request->input('type')=='other')
		 {
		 	$client = Client::create([
		 		'user_id'  =>  $user->id,
		 	]);
		 }
		 else {
		 	$professional = Professionals::create([
		    'user_id'  =>  $user->id,
		    'description'  =>  '',
		    'is_available'  =>  false,
		    'is_active'  =>   false ,

		]);
		 }

		 return redirect('/')->with('flash_message', 'Registration Complete');


	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		//
	}

	function checkIfExist($x)
	{
		$user = Privileges::firstOrNew(array('name' => $x));
		$user->name = $x;
		$user->save();
	}

	function checkIfExistVar($name,$value,$description)
	{
		$var = Variable::firstOrNew(array('name' => $name));
		$var->name = $name;
		$var->description = $description;
		if($value!='')
		$var->value = $value;
		$var->save();
	}

}
