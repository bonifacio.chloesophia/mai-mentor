<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Skills;
use Log;
use Validator;
class SkillsController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		//
		$skills = Skills::paginate(10);

		return view('skills.index',[
			'skills'=>$skills,
		]);

	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
		return view('skills.create',[
  
		]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		//
		// Log::info($request);

		$validator = Validator::make($request->all(), [
			'name' => 'required|min:2|max:50',
		 
		]);

		if ($validator->fails()) {
			return redirect()
				->route('skills.create')
						->withErrors($validator)
						->withInput();
		}
	 
		
		$user = Skills::firstOrCreate([
			'name'  =>  $request->input('name'),
			'type'  =>  $request->input('type'),

		]);

		 return redirect()->route('professionals.index')->with('flash_message', 'Good!');

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		//
		$skill = Skills::find($id);
		return view('skills.edit',[
			'skill'=>$skill,
		]);

	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		//
		$validator = Validator::make($request->all(), [
            'name' => 'required|min:2|max:50',
         
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('skills.edit',$id)
                        ->withErrors($validator)
                        ->withInput();
        }
     
        // Log::info($request);
        $skill = Skills::find($id); 
        $skill->name = $request->input('name');
        $skill->type = $request->input('type');
        $skill->save();

         return redirect()->route('skills.index')->with('flash_message', 'Updated!');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		//
	}
}
