<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Log;
use DB;
use App\Variable;
class SettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // Log::info('asd');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $software_name = DB::table('var')
            ->where('name','=','software_name')
            ->first();
        $company_name = DB::table('var')
            ->where('name','=','company_name')
            ->first();
        $company_address = DB::table('var')
            ->where('name','=','company_address')
            ->first();
        $company_phone = DB::table('var')
            ->where('name','=','company_phone')
            ->first();
        $company_email = DB::table('var')
            ->where('name','=','company_email')
            ->first();
        $company_logo = DB::table('var')
            ->where('name','=','company_logo')
            ->first();
        
        return view('settings.settings',[
            'software_name'=>$software_name,
            'company_name'=>$company_name,
            'company_address'=>$company_address,
            'company_phone'=>$company_phone,
            'company_email'=>$company_email,
            'company_logo'=>$company_logo,
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        Log::info($request);

        $i=0;
        for($i=0;$i<count($request->input('settings_name'));$i++)
        {
            $settings_name = $request->input('settings_name')[$i];
            $settings_id = $request->input('settings_id')[$i];
            if($settings_name=='company_logo')
            {

                 Log::info($request->file('photo'));
                if($request->file('photo')!='')
                {
                   
                    $destinationPath = 'uploads'; // upload path
                    $photoExtension = $request->file('photo')->getClientOriginalExtension(); 

                    $photoFileName = $request->input('settings_name')[$i].".".$photoExtension;
                    $request->file('photo')->move($destinationPath, $photoFileName);
                    $x = Variable::find($settings_id);
                    $x->value = $photoFileName;
                    $x->save(); 
                }
                    
            }
            else
            {
                $x = Variable::find($settings_id);
                $x->value = $request->input($settings_name);
                $x->save();     
            }
            
        }

        $var = Variable::all();
        foreach ($var as $x) {
            session([$x->name => $x->value]); 
        }
       
        return redirect()->route('settings.edit',[1])->with('flash_message', 'Settings Updated!!');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
