<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Project;
use App\PrintPaper;
use App\Tutorial;
use Log;
use Validator;
use Auth;
use Carbon\Carbon;
class PageController extends Controller
{
	//
	public function webmail()
	{
		return view('webmail',[]);
	}

	public function dashboard()
	{
		return view('dashboard',[]);
	}


	public function index()
	{
		return view('index',[]);
	}

	public function login()
	{
		return view('login',[]);
	}


	public function logout()
	{
		return view('login',[]);
	}


	public function getStarted()
	{
		return view('get_started',[]);
	}

	public function services()
	{
		return view('services',[]);
	}

	public function printForm()
	{
		return view('print.form',[]);
	}
	public function developmentPicker()
	{
		return view('development.picker',[]);
	}

	public function paperPicker()
	{
		return view('paper.picker',[]);
	}

	public function creativesPicker()
	{
		return view('creatives.picker',[]);
	}

	public function writingPicker()
	{
		return view('writing.picker',[]);
	}

	public function tutorForm()
	{
		return view('tutor.form',[]);
	}


	public function paperForm($type)
	{
		return view('paper.form',[
			'type'=>$type, 
		]);
	}

	public function developmentForm($type)
	{
		return view('development.form',[
			'type'=>$type, 
		]);
	}

	public function creativesForm($type)
	{
		return view('creatives.form',[
			'type'=>$type, 
		]);
	}

	public function writingForm($type)
	{
		return view('writing.form',[
			'type'=>$type, 
		]);
	}


	public function register()
	{
		return view('register',[]);
	}

	public function tutorSubmit(Request $request)
	{
		if(isset($_POST['g-recaptcha-response']) && !empty($_POST['g-recaptcha-response']))
		{
			$secret = '6LeIxAcTAAAAAGG-vFI1TnRWxMZNFuojJ4WifJWe';
			$verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
			$responseData = json_decode($verifyResponse);

			if($responseData->success)
			{
				$succMsg = 'Your contact request have submitted successfully.';
				Log::info($request);
				$validator = Validator::make($request->all(), [
		            'location' => 'required',		          	
		            'subject' => 'required',
		            'details' => 'required',
		            'date_of_tutorial' => 'required',
		            'people_count' => 'required|numeric',
		            'hours' => 'required|numeric',		            
		            'provisions' => 'required',		            
		            'contact_number' => 'required',		            
		         
		        ]);

		        if ($validator->fails()) {
		            return redirect()
		                
		                ->back()
		                ->withErrors($validator)
		                ->withInput();
		        }	
	

		        $tutorial = Tutorial::create([
		            'user_id' => Auth::user()->id,
		            'location'=> $request->location,
		            'subject'=> $request->subject,
		            'details'=> $request->details,
		            'people_count'=> $request->people_count,
		            'date_of_tutorial'=> Carbon::parse($request->date_of_tutorial)->format('Y-m-d H:i'),
		            'hours'=> $request->hours,
		            'provisions'=> $request->provisions,
		            'contact_number'=> $request->contact_number,
		        ]);

		        return redirect()->route('index')->with('flash_message', 'Tutorial Submitted');

			}
			else
			{
				$errMsg = 'Robot verification failed, please try again.';
				Log::info($errMsg);
			}
		}
		
	}

	public function printSubmit(Request $request)
	{
		if(isset($_POST['g-recaptcha-response']) && !empty($_POST['g-recaptcha-response']))
		{
			$secret = '6LeIxAcTAAAAAGG-vFI1TnRWxMZNFuojJ4WifJWe';
			$verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
			$responseData = json_decode($verifyResponse);

			if($responseData->success)
			{
				$succMsg = 'Your contact request have submitted successfully.';
				Log::info($request);
				Log::info(Auth::user()->id);


				 $validator = Validator::make($request->all(), [
		            'paper_size' => 'required',		          	
		            'paper_type' => 'required',
		            'copies' => 'required|numeric',
		            'color' => 'required',
		            'instructions' => 'required',
		            'file' => 'required',
		         
		        ]);

		        if ($validator->fails()) {
		            return redirect()
		                ->route()
		                ->back()
		                ->withErrors($validator)
		                ->withInput();
		        }	

		        $destinationPath = 'uploads/print';
		        $photoExtension = $request->file('file')->getClientOriginalExtension(); 
		        $file = 'file'.uniqid().'.'.$photoExtension;
		        $request->file('file')->move($destinationPath, $file);


		        $proj = PrintPaper::create([
		            'user_id' => Auth::user()->id,
		            'original_file_name' =>$request->file('file')->getClientOriginalName(),
		            'file_name' => $file,
		            'instructions' => $request->input('instructions'),
		            'paper_size'=> $request->paper_size,
		            'paper_type'=> $request->paper_type,
		            'copies'=> $request->copies,
		            'color'=> $request->color,
		        ]);

		        return redirect()->route('index')->with('flash_message', 'Print Submitted');

			}
			else
			{
				$errMsg = 'Robot verification failed, please try again.';
				Log::info($errMsg);
			}
		}
		
	}
	public function formSubmit(Request $request)
	{
		// Log::info($request);


		if(isset($_POST['g-recaptcha-response']) && !empty($_POST['g-recaptcha-response']))
		{
			$secret = '6LeIxAcTAAAAAGG-vFI1TnRWxMZNFuojJ4WifJWe';
			$verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
			$responseData = json_decode($verifyResponse);

			if($responseData->success)
			{
				$succMsg = 'Your contact request have submitted successfully.';
				Log::info($request);
				Log::info(Auth::user()->id);


				 $validator = Validator::make($request->all(), [
		            'title' => 'required|min:2|max:50',		          	
		            'description' => 'required',
		         
		        ]);

		        if ($validator->fails()) {
		            return redirect()
		                ->route()
		                ->back()
		                ->withErrors($validator)
		                ->withInput();
		        }

		        $proj = Project::create([
		            'title'  =>  $request->input('title'),
		            'user_id' => Auth::user()->id,
		            'description' => $request->input('description'),
		            'type'=> $request->type,
		            'status'=> 'review',
		        ]);

		        return redirect()->route('index')->with('flash_message', 'Projects Created!');

			}
			else
			{
				$errMsg = 'Robot verification failed, please try again.';
				Log::info($errMsg);
			}
		}



		// return view('development.form',[
		// 	'type'=>$type, 
		// ]);
	}

	

	public function registerSubmit(Request $request)
	{
		// Log::info($request);
		// if(isset($_POST['g-recaptcha-response']) && !empty($_POST['g-recaptcha-response']))
		// {
		// 	$secret = '6LeIxAcTAAAAAGG-vFI1TnRWxMZNFuojJ4WifJWe';
		// 	$verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
		// 	$responseData = json_decode($verifyResponse);

		// 	if($responseData->success)
		// 	{
		// 		$succMsg = 'Your contact request have submitted successfully.';
		// 		Log::info($succMsg);
		// 	}
		// 	else
		// 	{
		// 		$errMsg = 'Robot verification failed, please try again.';
		// 		Log::info($errMsg);
		// 	}
		// }



		// return view('development.form',[
		// 	'type'=>$type, 
		// ]);
	}
}
