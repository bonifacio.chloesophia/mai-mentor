<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Professionals;
use App\User;
use Log;
use Validator;
use Hash;
class ProfessionalsController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$professionals = Professionals::has('user')
					->with('user')
					->paginate(10);

		return view('professionals.index',[
			'professionals'=>$professionals,
		]);

		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
		return view('professionals.create',[
  
		]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		//
		// Log::info($request);
		$validator = Validator::make($request->all(), [
			'first_name' => 'required|min:2|max:50',
			'last_name' => 'required|min:2|max:50',
			'email' => 'email|required|min:2|max:100|unique:users',
			'contact_no' => 'required|min:2|max:50',
			'password' => 'required|min:4|max:50',
			'password_confirmation' => 'required|same:password',
		 
		]);

		if ($validator->fails()) {
			return redirect()
				->route('professionals.create')
						->withErrors($validator)
						->withInput();
		}
	 	
	 	$user = User::create([
			'first_name'  =>  $request->input('first_name'),
			'last_name' => $request->input('last_name'),
			'email' => $request->input('email'),
			'contact_no' => $request->input('contact_no'),
			'password' => Hash::make($request->input('password')),
			'type' => 'professional',

		]);

		
		$professional = Professionals::create([
		    'user_id'  =>  $user->id,
		    'description'  =>  $request->input('description'),
		    'is_available'  =>  (null==$request->input('is_available')) ? false : true ,
		    'is_active'  =>   (null==$request->input('is_active')) ?false : true ,

		]);

		 return redirect()->route('professionals.index')->with('flash_message', 'Successfully added!');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//

	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		//

		$professional = Professionals::with('user')->find($id);
		return view('professionals.edit',[
			'professional'=>$professional,
		]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		//
		$professional = Professionals::with('user')->find($id);

		$validator = Validator::make($request->all(), [
			'first_name' => 'required|min:2|max:50',
			'last_name' => 'required|min:2|max:50',
			'email' => 'email|required|min:2|max:100|unique:users,email,'.$professional->user->id,
			'contact_no' => 'required|min:2|max:50',
			// 'password' => 'required|min:4|max:50',
			// 'password_confirmation' => 'required|same:password',
		 
		]);

		if ($validator->fails()) {
			return redirect()
				->route('professionals.edit',$id)
						->withErrors($validator)
						->withInput();
		}
	 	
	 	$professional->description = $request->input('description');
	 	$professional->is_available = (null==$request->input('is_available')) ? false : true;
	 	$professional->is_active = (null==$request->input('is_active')) ? false : true ;
	 	$professional->save();

	 	$user = User::find($professional->user_id);
	 	$user->first_name = $request->input('first_name');
	 	$user->last_name = $request->input('last_name');
	 	$user->email = $request->input('email');
	 	$user->contact_no = $request->input('contact_no');
	 	$user->save();


		 return redirect()->route('professionals.index')->with('flash_message', 'Updated!');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		//
	}
}
