<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Project;
use App\Professionals;
use App\ProjectProfessional;
use App\ProjectMilestone;
use Log;
use Validator;

class ProjectMilestonesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function doneMilestone($project_id,$id)
    {
        $pm = ProjectMilestone::where('id',$id)->first();
        $pm->done = ($pm->done==0) ? 1 : 0;
        $pm->save();


        return redirect()->back()->with('flash_message', 'Milestone Updated');
    }
    public function index($project_id)
    {
        //
        $project = Project::with('user')->find($project_id);
        // $professionals = Professionals::with('user')->get();
        $pp = ProjectProfessional::with('user')->where('project_id','=',$project_id)->get();
        $pm = ProjectMilestone::with('professionals')->where('project_id','=',$project_id)->get();

        // Log::info($pm);
        
        // $skills = Skills::get();
        // $skillsCollection = collect();
        // foreach($pp as $x) 
        // {
        //     foreach($x->user->userSkills as $e) 
        //     {
        //         $g = Skills::find($e->skill_id);
        //         // if(!$c->contains($g))
        //         // {
        //             $skillsCollection->push($g);
        //         // }
        //     }
          
        // }

        return view('projects.milestone.index',[
            'project'=>$project,
            // 'professionals'=>$professionals,
            'pp'=>$pp,
            'pm'=>$pm,
            // 'skills_collection'=>$skillsCollection,
            // 'skills'=>$skills,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($project_id)
    {

        $project = Project::with('user')->find($project_id);
        $pp = ProjectProfessional::with('user')
        // ->join('professionals', 'project_professionals.user_id', '=', 'professionals.id')
        ->where('project_id','=',$project_id)->get();
        // Log::info($pp);
        return view('projects.milestone.create',[
            'project'=>$project,
            'pp'=>$pp,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$project_id)
    {
        //
        // Log::info($request);
      
        $validator = Validator::make($request->all(), [
            'name' => 'required|min:2|max:50',
            'start' => 'required|date',
         
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('professionals.milestone.create',$project_id)
                        ->withErrors($validator)
                        ->withInput();
        }
        $user = Professionals::where('user_id','=',$request->input('user_id'))->first();
   
        // Log::info($user);
        $milestone = ProjectMilestone::create([
            'project_id' => $project_id,
            'name'  =>  $request->input('name'),
            'start' => $request->input('start'),
            'end' => $request->input('end'),
            'professional_id' => ($request->input('user_id')) ? $user->id : null,

        ]);

 

         return redirect()->route('projects.milestones.index',$project_id)->with('flash_message', 'Good!');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id,$project_id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id,$project_id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id,$project_id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,$project_id)
    {
        //
    }

    public function deleteMilestone($project_id,$id)
    {
        ProjectMilestone::where('id',$id)->delete();
        return redirect()->back()->with('flash_message', 'Milestone Removed');
    }
}
