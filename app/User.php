<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

use App\Professionals;
use App\Rating;
use App\UserRoles;
use App\Privileges;
use App\RolePrivileges;
use Log;
class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'contact_no',
        'password',
        'type',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

     public function userSkills()
    {
        return $this->hasMany('App\UserSkills');
    }

    public function roles()
    {
        return $this->hasMany('App\UserRoles');
    }

    public function Professionals()
    {
        return $this->hasOne('App\Professionals');
    }

    public function checkPrivileges($privileges)
    {
        $id = $this->id;
        $userRoles = UserRoles::where('user_id',$id)->first();
        $role_id = $userRoles== '' ? 0 : $userRoles->role_id;
        $Privileges = Privileges::where('name',$privileges)->first();
        
        $RolePrivileges = RolePrivileges::where('role_id',$role_id)
            ->where('privilege_id',$Privileges->id)
            ->get();
        return count($RolePrivileges)>0 ? 1 : 0;
        
    }

    public function isRole($role_name)
    {
        // Log::info($role_name);
        $id = $this->id;
        $userRoles = UserRoles::where('user_id',$id)->first();
        Log::info($userRoles);
        if($userRoles)
        {
            $role = Roles::where('id',$userRoles->role_id)
                ->where('role_name',$role_name)->get();
            return count($role)>false ? true : false;
        }
        return false;
        
    }

    public function projectRating($project_id)
    {
        $id = $this->id;
        $pp = Professionals::where('user_id','=',$id)->first();
        $p = Rating::where('project_id','=',$project_id)
            ->where('professional_id','=',$pp->id)
            ->first();

        return ($p) ? $p->rating : 0;
    }
}
