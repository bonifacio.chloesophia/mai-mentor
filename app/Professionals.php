<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Project;
use App\Rating;
class Professionals extends Model
{
    //
    protected $fillable = [
		'user_id',
		'description',
		'resume',
		'is_available',
		'is_active',
	];

	protected $table = 'professionals';

	public function user()
	{
		return $this->belongsTo('App\User');
	}

	public function skills()
	{
		return $this->belongsTo('App\skills');
	}

	public function rating()
	{
		$id = $this->id;
		$count = Rating::where('professional_id','=',$id)->avg('rating');
		return round($count);
	}



}
