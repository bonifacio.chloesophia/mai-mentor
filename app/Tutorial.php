<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tutorial extends Model
{
    //
     protected $fillable = [
		'user_id',
		'location',
		'subject',
		'details',
		'people_count',
		'hours',
		'provisions',
		'contact_number',
		'date_of_tutorial',
	];

	protected $table = 'tutorials';

}
