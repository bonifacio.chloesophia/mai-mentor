<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    //
     protected $fillable = [
		'title',
		'user_id',
		'description',
		'type',
		'status',
		'project_type',
	];

	// protected $table = 'clients';

	public function user()
	{
		return $this->belongsTo('App\User');
	}
}
