<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    //
    protected $fillable = [
		'user_id',
		'is_active',
	];

	protected $table = 'clients';

	public function user()
	{
		return $this->belongsTo('App\User');
	}
}
