<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectProfessional extends Model
{
    //
    protected $fillable = [
		'project_id',
		'user_id',
	];

	// protected $table = 'clients';

	public function user()
	{
		return $this->belongsTo('App\User');
	}
}
