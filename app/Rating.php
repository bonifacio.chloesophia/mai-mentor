<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{
    //
     //
    protected $fillable = [
		'project_id',
		'professional_id',
		'rating',
	];

	// protected $table = 'clients';

	public function professionals()
	{
		return $this->belongsTo('App\Professionals','professional_id');
	}
	public function project()
	{
		return $this->belongsTo('App\Project');
	}
}
