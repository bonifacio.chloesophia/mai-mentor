<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PrintPaper extends Model
{
    //
    protected $fillable = [
		'user_id',
		'original_file_name',
		'file_name',
		'paper_size',
		'paper_type',
		'color',
		'copies',
		'instructions',
	];

	protected $table = 'prints';

	
}
