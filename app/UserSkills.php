<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserSkills extends Model
{
    //
    protected $fillable = [
		'user_id',
		'skill_id',
	];

	protected $table = 'user_skills';
	public $timestamps = false;
	
	public function user()
	{
		return $this->belongsTo('App\User');
	}
	public function skill()
	{
		return $this->belongsTo('App\Skills');
	}
}
