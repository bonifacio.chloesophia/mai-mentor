<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Privileges;
use App\Roles;
use App\Skills;
class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(MigrationTable::class);
    }
}
class MigrationTable extends Seeder 
{
	public function run()
	{	

		
		$user = User::create([
			'email'	=>	'richard.lacasandile@gmail.com',
			'password'	=>	Hash::make('111111'),
			'first_name'	=>	'gabu',
			'middle_name'	=>	'gabu',
			'last_name'	=>	'gabu',
			'contact_no'=>	'7493345',
			'type'=>	'admin',
			]);


		DB::table('var')->insert([
				'name' => 'software_name',
				'value' => 'PROJECT RUSH',
				'description' => 'Software Name',
				]);
		DB::table('var')->insert([
			'name' => 'company_address',
			'value' => '123 123',
			'description' => 'Company Address',
			]);
		DB::table('var')->insert([
			'name' => 'company_phone',
			'value' => '7434525',
			'description' => 'Company Phone',
			]);
		DB::table('var')->insert([
			'name' => 'company_email',
			'value' => 'pogipol@gmail.com',
			'description' => 'Company Email',
			]);
		DB::table('var')->insert([
			'name' => 'company_logo',
			'value' => 'company_logo.png',
			'description' => 'Company Logo',
			]);



		$roles = Roles::create([
			'role_name' => 'superuser',
			]);

	
		DB::table('user_roles')->insert([
			'user_id' => $user->id,
			'role_id' => $roles->id,
			]);

		$is_allow_settings = Privileges::create([
			'name' => 'is_allow_settings',
			]);
		DB::table('role_privileges')->insert([
			'role_id' => $roles->id,
			'privilege_id' => $is_allow_settings->id,
			]);
		
		$is_allow_user = Privileges::create([
			'name' => 'is_allow_user',
			]);
		DB::table('role_privileges')->insert([
			'role_id' => $roles->id,
			'privilege_id' => $is_allow_user->id,
			]);

		$is_allow_skills = Privileges::create([
			'name' => 'is_allow_skills',
			]);
		DB::table('role_privileges')->insert([
			'role_id' => $roles->id,
			'privilege_id' => $is_allow_skills->id,
			]);

		$is_allow_professionals = Privileges::create([
			'name' => 'is_allow_professionals',
			]);
		DB::table('role_privileges')->insert([
			'role_id' => $roles->id,
			'privilege_id' => $is_allow_professionals->id,
			]);

		$is_allow_clients = Privileges::create([
			'name' => 'is_allow_clients',
			]);
		DB::table('role_privileges')->insert([
			'role_id' => $roles->id,
			'privilege_id' => $is_allow_clients->id,
			]);

		$is_allow_projects = Privileges::create([
			'name' => 'is_allow_projects',
			]);
		DB::table('role_privileges')->insert([
			'role_id' => $roles->id,
			'privilege_id' => $is_allow_projects->id,
			]);

		Skills::create([
			'name' => 'Photoshop',
			'type' => 'design',
		]);
		Skills::create([
			'name' => 'C#',
			'type' => 'coding',
		]);
		Skills::create([
			'name' => 'Php',
			'type' => 'coding',
		]);
		Skills::create([
			'name' => 'QA',
			'type' => 'qa',
		]);

		Skills::create([
			'name' => 'docs',
			'type' => 'docs',
		]);

		// $is_admin = Privileges::create([
		// 	'name' => 'is_admin',
		// 	]);
		// DB::table('role_privileges')->insert([
		// 	'role_id' => $roles->id,
		// 	'privilege_id' => $is_admin->id,
		// 	]);
		
		factory(App\Client::class,20)->create()->each(function($post){
				$post->save();
			});

		factory(App\Professionals::class,5)->create()->each(function($post){
				$post->save();
			});

		
	}
}
