<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTurorialTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tutorials', function (Blueprint $table) {            //
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('location');
            $table->decimal('lat',18,4)->nullable();
            $table->decimal('lng',18,4)->nullable();
            $table->string('subject');
            $table->mediumText('details')->nullable();
            $table->tinyInteger('people_count')->default(1);
            $table->enum('provisions',['food','honorarium','certificate','none'])->default('none');
            $table->enum('status',['done','waiting','cancelled'])->default('waiting');
            $table->tinyInteger('hours');
            $table->dateTime('date_of_tutorial');
            $table->string('contact_number');
            $table->timestamps();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tutorials');
    }
}
