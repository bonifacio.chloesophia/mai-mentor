<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrintTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prints', function (Blueprint $table) {            //
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('original_file_name');
            $table->string('file_name');
            $table->enum('paper_size',['letter','legal','a4','others']);
            $table->enum('paper_type',['plain','glossy']);
            $table->enum('color',['color','greyscale']);
            $table->tinyInteger('copies')->default(1);
            $table->mediumText('instructions')->nullable();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('prints');
    }
}
