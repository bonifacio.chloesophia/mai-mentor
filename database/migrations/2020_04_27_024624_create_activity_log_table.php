<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivityLogTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('activity_log', function (Blueprint $table) {
			$table->increments('id');
			$table->string('username');
			$table->string('entry', 1000);
			$table->string('comment');
			$table->enum('family', array('void','insert','update','delete','login-success','login-failure','logout','others'))->default('others');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('activity_log');
	}
}
