<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->String('title')->nullable();
            $table->mediumText('description')->nullable();
            $table->enum('status',['active','inactive','wip','completed','review'])->default('review');
            $table->enum('project_type',['development','creatives','writing','paper','printing']);
            $table->enum('type',['web','mobile','software','paper','art','animation','print','video','photo','presentation','other','content_writing','creative_writing','proofread','qualitative','quantitative','mixed_paper']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('projects');
    }
}
