<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker,$user_type) {

	// Log::info($user_type);
	$type=array("student","other");
	$rnd_type=array_rand($type);
    return [
        'first_name' => $faker->firstName,
        'middle_name' => $faker->lastName,
        'last_name' => $faker->lastName,
        'contact_no' => $faker->phoneNumber,
        // 'type' => $type[$rnd_type],
        'email' => $faker->safeEmail,
        'password' => bcrypt('password'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Client::class, function (Faker\Generator $faker) {

	$type=array("student","other");
	$rnd_type=array_rand($type);
	$user =factory('App\User')->create([
			    // 'type' => 'client',
			    'first_name' => $faker->firstName,
		        'middle_name' => $faker->lastName,
		        'last_name' => $faker->lastName,
		        'contact_no' => $faker->phoneNumber,
		        'type' => $type[$rnd_type],
		        'email' => $faker->safeEmail,
		        'password' => bcrypt('password'),
		        'remember_token' => str_random(10),
			]);
    return [
        'user_id' => $user->id,
    	// 'user_id' => ->id,
        'is_active' => 1,
    ];
});


$factory->define(App\Professionals::class, function (Faker\Generator $faker) {

	
	$user =factory('App\User')->create([
			    // 'type' => 'client',
			    'first_name' => $faker->firstName,
		        'middle_name' => $faker->lastName,
		        'last_name' => $faker->lastName,
		        'contact_no' => $faker->phoneNumber,
		        'type' => 'professional',
		        'email' => $faker->safeEmail,
		        'password' => bcrypt('password'),
		        'remember_token' => str_random(10),
			]);


    return [
        // 'user_id' => $faker->firstName,
    	'user_id' => $user->id,
        'is_available' => 1,
        'is_active' => 1,
        'description' => 'description',
        'resume' => 'resume',
    ];
});

